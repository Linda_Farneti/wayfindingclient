import 'mocha';
import { expect } from 'chai';
import { Roads } from '../utility/roads';
import { RoadDto } from '../model/road-dto';
import { List } from '../utility/list';

let nodes = [{"Id":0,"Latitude":44.402038,"Longitude":11.509791},
            {"Id":1,"Latitude":44.401678,"Longitude":11.509437},
            {"Id":2,"Latitude":44.401299,"Longitude":11.509464},
            {"Id":3,"Latitude":44.400713,"Longitude":11.508881},
            {"Id":4,"Latitude":44.400495,"Longitude":11.508485}];

let roads = [{"ID":0,"Length":48.921489976241155,"StartNode":0,"EndNode":1,"Size":10},
            {"ID":1,"Length":42.19742929529782,"StartNode":1,"EndNode":2,"Size":10},
            {"ID":2,"Length":79.94393896295004,"StartNode":2,"EndNode":3,"Size":10},
            {"ID":3,"Length":39.71580196309002,"StartNode":3,"EndNode":4,"Size":10}];

describe('getRoadsFromNode function', () => {
    it('should return the roads with id 1 and 2', () => {
        let result = Roads.getRoadsFromNode(nodes[2], roads);
        expect(result).to.deep.equal(new List<RoadDto>(new Array(roads[1], roads[2])));
    });
});

describe('getRoadsFromNode not found function', () => {
    it('should return an empty list', () => {
        let result = Roads.getRoadsFromNode({Id: 8, Latitude: 45.89756, Longitude: 34.467463}, roads);
        expect(result).to.deep.equal(new List<RoadDto>());
    });
});

describe("getRoadBetweenNodes function", () => {
    it('should return the road 3', () => {
        let result = Roads.getRoadBetweenNodes(nodes[3], nodes[4], roads);
        expect(result).to.deep.equal(roads[3]);
    })
})

describe("getRoadBetweenNodes function", () => {
    it('should return undefined', () => {
        let result = Roads.getRoadBetweenNodes({Id: 12, Latitude: 45.89756, Longitude: 34.467463}, {Id: 8, Latitude: 45.89756, Longitude: 31.467463}, roads);
        expect(result).to.deep.equal(undefined);
    })
})

describe("getRoadBetweenNodes function", () => {
    it('should return undefined', () => {
        let result = Roads.getRoadBetweenNodes(nodes[4], {Id: 8, Latitude: 45.89756, Longitude: 31.467463}, roads);
        expect(result).to.deep.equal(undefined);
    })
})

describe("getRoadBetweenNodes function", () => {
    it('should return undefined', () => {
        let result = Roads.getRoadBetweenNodes(nodes[0], nodes[3], roads);
        expect(result).to.deep.equal(undefined);
    })
})