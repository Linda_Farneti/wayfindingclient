import 'mocha';
import { DistanceCalculator } from "../utility/distance-calculator";
import { expect } from 'chai';
import { Nodes } from "../utility/nodes";

let nodes = [{"Id":0,"Latitude":44.402038,"Longitude":11.509791},
         {"Id":1,"Latitude":44.401678,"Longitude":11.509437}, 
         {"Id":2,"Latitude":44.401299,"Longitude":11.509464}, 
         {"Id":3,"Latitude":44.400713,"Longitude":11.508881}];

describe('getDistanceNodeToNode function', () => {
    it('should return 18617 (distance between Forli and Cesena)', () => {
        const result = DistanceCalculator.getDistanceNodeToNode(44.22255 , 12.04100, 44.13722, 12.24187);
        expect(Math.round(result)).to.equal(18617);
    });
});

describe('getDistanceNodeToNode function', () => {
    it('should return 0', () => {
        const result = DistanceCalculator.getDistanceNodeToNode(44.22255 , 12.04100, 44.22255 , 12.04100);
        expect(Math.round(result)).to.equal(0);
    });
});

describe('getNodeFromID not found function', () => {
    it('should return error', () => {
        let result = Nodes.getNodeFromID(5, nodes);
        expect(result).to.not.exist;
    });
});

describe('getNodeFromID function', () => {
    it('should return error', () => {
        let result = Nodes.getNodeFromID(2, nodes);
        expect(result).equal(nodes[2]);
    });
});

describe('nearestNode function', () => {
    it('should return the node 2', () => {
        let result = Nodes.nearestNode(44.401298,11.509465, nodes);
        expect(result).to.deep.equal(Nodes.getNodeFromID(2, nodes));
    });
});

describe('getCoordinates not found function', () => {
    it('should return error', () => {
        let result = Nodes.getCoordinates(8, nodes);
        expect(result).to.deep.equal(new Array());
    });
});

describe('getCoordinates function', () => {
    it('should return {44.400713,11.508881}', () => {
        let result = Nodes.getCoordinates(3, nodes);
        expect(result).to.deep.equal(new Array(44.400713,11.508881));
    });
});