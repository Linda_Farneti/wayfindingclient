import { MapObserver } from "../controller/map-observer";
import { PoiDto } from "../model/poi-dto";
import { NodeDto } from "../model/node-dto";
import { Pair } from "../utility/pair";

export interface Drawer {
    drawMap(data: Array<Array<any>>) : void;
    findPath(data: Array<any>): void;
    hidePoi(id: string): Array<Pair<string, string>>;
    viewAll(): void;
    addObserver(obs: MapObserver): void;
    onPositionUpdate(x: number, y:number): void;
    optionClicked(s: string): void;
    positioningOn(): void;
    positioningOff(): void;
    setCoordinates(lat: number, lon: number): void;
}