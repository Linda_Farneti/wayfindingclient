import * as L from "leaflet";
import { Drawer } from "./drawer";
import { Map } from "../utility/map";
import { ArrayMap } from "../utility/array-map";
import { NodeDto } from "../model/node-dto";
import { PoiDto } from "../model/poi-dto";
import { RoadDto } from "../model/road-dto";
import { IList } from "../utility/i-list";
import { MapObserver } from "../controller/map-observer";
import { List } from "../utility/list";
import { Pair } from "../utility/pair";
import { PairImpl } from "../utility/pair-impl";
import { DistanceCalculator } from "../utility/distance-calculator";
import { GpsObserver } from "../controller/gps-observer";
import { Consts } from "../utility/consts";
import { Nodes } from "../utility/nodes";
import { Position } from "../utility/position";
import { PathCalculator } from "../utility/path-calculator";
import { Pois } from "../utility/pois";
import { injectable } from "inversify";
import "reflect-metadata";
import TYPES from "../utility/types";
import container from "../utility/inversify.config";

@injectable()
export class MapDrawer implements Drawer {
    private positioning: boolean;
    private map: any;
    private nodes: NodeDto[];
    private poi: Map<PoiDto, L.Marker>;
    private roads: Map<RoadDto, L.Polyline>;
    private path: Map<RoadDto, L.Polyline>;
    private poiSelected: number;
    private pointSelector: L.Marker;
    private mapObserver: IList<MapObserver>;
    //private lastCrossed: Pair<NodeDto, L.Polyline>; //ultimo nodo attraversato e strada sulla quale mi trovo (in teoria)
    private _gps: GpsObserver;

    private lat: number;
    private lon: number;

    public constructor(map: any, data: Array<Array<any>>){
        this.positioning = null;
        this.nodes = new Array<NodeDto>();
        this.roads = new ArrayMap<RoadDto, L.Polyline>();
        this.poi = new ArrayMap<PoiDto, L.Marker>();
        this.path = new ArrayMap<RoadDto, L.Polyline>();
        this.map = map;
        this.drawMap(data);
        this.mapObserver = new List<MapObserver>();
        this._gps = container.get<GpsObserver>(TYPES.GpsObserver);

        //this.lat = this._gps.getActualPosition().getX();
        //this.lon = this._gps.getActualPosition().getY();

        //let actualPos = this.gps.getActualPosition().getX();
        /*
        let actualLat = this.gps.getActualLatitude();
        $("#utile").text("Lat: " + actualLat);
        let actualLong = this.gps.getActualLongitude();
        this.setNewPosition(actualLat, actualLong);
        */
    }

    private drawPoi(c: PoiDto) {
        let coord = Array();
        coord.push(Nodes.getCoordinates(c.NodeID, this.nodes));

        let icon = L.icon({
            //iconUrl: "http://" + Consts.IP_SERVER + "/Wayfinding/Images/" + c.Icon + ".svg",
            iconUrl: "http://" + Consts.IP_CLIENT + "/images/" + c.Icon + ".svg",
            iconSize: [c.Size, c.Size]
        })

        let marker = L.marker(coord[0]);
        marker.setIcon(icon);
        marker.addTo(this.map);

        marker.on("click", () => {
            this.optionClicked(c.Name);
        });  

        this.poi.push(c, marker);
    }

    private drawRoad(r: any) {
        let coord = Array();
        coord.push(Nodes.getCoordinates(r.StartNode, this.nodes));
        coord.push(Nodes.getCoordinates(r.EndNode, this.nodes));
        let newLine = L.polyline(coord, {
            color: "white",
            weight: r.Size
        }).addTo(this.map);
        this.roads.push(r, newLine); 
    }

    private resetPath() {
        this.path.entries().list().forEach(r => {
            this.map.removeLayer(r.getY());
        });
        this.path = new ArrayMap<RoadDto, L.Polyline>();
    }
    
    private underlineRoad(r: RoadDto) {
        let coord = Array();
        coord.push(Nodes.getCoordinates(r.StartNode, this.nodes));
        coord.push(Nodes.getCoordinates(r.EndNode, this.nodes));
        let newLine = L.polyline(coord, {
            color: "green",
            lineJoin: 'round',
            weight: 5,
            opacity: 1,
        }).addTo(this.map);
        this.path.push(r, newLine);
    }

    /*private updatePath(lat: number, long: number) {
        let t: boolean;
        let coord = Array();
        coord.push([lat, long]);
        this.path.entries().list().forEach(p => {
            if (p.getX().StartNode == this.lastCrossed.getX().Id) {
                t = true;
            }
            if (t == true) {
                coord.push(p.getY().getLatLngs()[1]);
            }
        });
        this.hidePath();
        this.lastCrossed.getY().setLatLngs(coord).setStyle({
            color: "green"
        });
        this.lastCrossed.getY().bringToFront(); 
    } */

    private hidePath(): void {
        this.path.entries().list().forEach(p => {
            p.getY().setStyle({
                color: "white"
            })
        });
    }

    private setNewPosition(x: number, y: number) {
        this.lat = x;
        this.lon = y;
        this.moveMarker();
    }

    private moveMarker() {
        if (this.pointSelector != undefined && this.pointSelector != null) {
            this.map.removeLayer(this.pointSelector);
        }
        let pointIcon = L.icon({
            iconUrl: "./images/positionMarker2.png",
            iconSize: [60, 60]
        });
        this.pointSelector = L.marker([this.lat, this.lon]);
        this.pointSelector.setIcon(pointIcon);
        this.pointSelector.addTo(this.map);
        if (this.positioning) {
            this.mapViewUpdate();
        }
    }

    private setDefaultView() {
        this.map.setZoom(16.4);
    }

    private mapViewUpdate() {
        this.map.setView(new L.LatLng(this.lat, this.lon), 19);
        this.map.dragging.enable;
    }

    public optionClicked(s: string) {
        let p = Pois.searchPoiFromName(s, this.poi);
        this.mapObserver.list().forEach(obs => {
            obs.onPoiClicked(new PairImpl<NodeDto, PoiDto>(Position.getPosition(this.nodes), p));
        });
        this.setDefaultView();
    }

    public drawMap(data: Array<Array<any>>) : void {
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: 19
        }).addTo(this.map);

        data[0].forEach(n => {
            let newPoint: NodeDto = {Id: n.Id, Latitude: n.Latitude, Longitude: n.Longitude};
            this.nodes.push(newPoint);
        });

        data[1].forEach(r => {
            this.drawRoad(r);
        })

        data[2].forEach(p => {
            this.drawPoi(p);
        })

    }

    public findPath(data: Array<RoadDto>): void {
        this.resetPath();
        data.forEach(n => {
            this.underlineRoad(n);
        });
        for(let i = 0; data[i] != null; i++) {
            this.underlineRoad(data[i]);
        }
        this.setNewPosition(Position.getPosition(this.nodes).Latitude, Position.getPosition(this.nodes).Longitude);
        //this.lastCrossed = new PairImpl(this.nodes[0], this.path.entries().list()[0].getY());
    }
    
    public onPositionUpdate(x: number, y: number) {
        this.setNewPosition(x, y);
        for (let i = 0; i < this.nodes.length; i++) {
            //da sistemare in base all'algoritmo di correzione
            //if (this.nearestNode(x, y) == this.nodes[i] && this.nodes[i] != this.lastCrossed.getX()) {
            if (this.nodes[i].Latitude == x && this.nodes[i].Longitude == y) {
                //this.lastCrossed = new PairImpl(this.nodes[i],this.searchPolyFromStartNode(this.nodes[i].Id));
            }
        }
        //this.updatePath(x, y);
    }

    public hidePoi(id: string): Array<Pair<string, string>> {
        let arr = new Array<Pair<string, string>>();
        this.resetPath();
        this.poi.entries().list().forEach(p => {
            p.getY().addTo(this.map);
            
            //bisogna scegliere come fare le ricerche, per ora risultano anche con singole lettere
            if (p.getX().Name.toLowerCase().indexOf(id.toLowerCase()) < 0) {
            //if (p.getX().Name.substr(0, id.length).toLowerCase() != id) {
                this.map.removeLayer(p.getY());
            } else {
                let tmpPath = PathCalculator.getPath(Position.getPosition(this.nodes).Id, p.getX().NodeID, this.nodes, this.roads.keys().list());
                let dist = 0;
                tmpPath.forEach(r => {
                    dist += DistanceCalculator.getDistanceNodeToNode(Nodes.getCoordinates(r.StartNode, this.nodes)[0],
                        Nodes.getCoordinates(r.StartNode, this.nodes)[1], Nodes.getCoordinates(r.EndNode, this.nodes)[0],
                        Nodes.getCoordinates(r.EndNode, this.nodes)[1]);
                });
                arr.push(new PairImpl(p.getX().Name," (" + Math.round(dist) + "m)"));
            }
        });
        return arr;
    }

    public setCoordinates(lat: number, lon: number): void {
        this.lat = lat;
        this.lon = lon;
    }

    public viewAll(): void {
        this.poi.entries().list().forEach(p => {
            p.getY().addTo(this.map);
        });
    }

    public addObserver(obs: MapObserver): void {
        this.mapObserver.add(obs);
    }

    public positioningOn() {
        this.positioning = true;
        this.mapViewUpdate();
    }

    public positioningOff() {
        this.positioning = false;
    }
}