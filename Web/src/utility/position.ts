import { NodeDto } from "../model/node-dto";
import { Nodes } from "./nodes";
import container from "./inversify.config";
import { GpsObserver } from "../controller/gps-observer";
import TYPES from "./types";

export class Position {
    static getPosition(nodes: NodeDto[]): NodeDto {
        let _gps = container.get<GpsObserver>(TYPES.GpsObserver);
        let node : NodeDto;
        node = Nodes.nearestNode(_gps.getActualPosition().getX(), _gps.getActualPosition().getY(), nodes);
        return node;
    }
}