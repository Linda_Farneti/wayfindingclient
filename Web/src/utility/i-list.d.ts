export interface IList<T>{
    contains(e : T) : boolean;
    containsAll(toCheck : Array<T>) : boolean;
    remove(e:T) : T;
    removeIndex(index : number) : T;
    removeAll(toRemove : Array<T>) : Array<T>;
    add(e:T) : void;
    addAll(toAdd : Array<T>) : void;
    clear() : void;
    indexOf(e : T) : number;
    replace(toRemove : T , toAdd : T) : T;
    list() : Array<T>;
    get(index : number) : T;
    size() : number;
    isEmpty() : boolean;
}