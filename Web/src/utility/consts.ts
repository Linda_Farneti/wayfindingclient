export enum State {DRAW_NODE, SELECT_PATH, SELECTION, MOVE};
export enum Item {ROAD, ICON, NODE};
export class Consts {
    static readonly LATITUDE =  44.40262;
    static readonly LONGITUDE = 11.50949; 
    static readonly CIRCLE_RAY = 10;
    static readonly MAP_FOCUS = 17;
    static readonly ROAD_WIDTH = 10;
    static readonly OPACITY = 1;
    static readonly DEFAULT_ICON_SIZE = 20;
    static readonly ICON_PATH = "./src/view/images/";
    static readonly ICON_EXT = ".svg";
    static readonly IP_SERVER = "192.168.0.7";
    static readonly IP_CLIENT = "192.168.1.81:5500";
    static readonly MAP_ID = 89;
    
    static readonly DEFAULT_NODE_STYLE = {
        color: "grey"
    }

    static readonly DEFAULT_ROAD_STYLE = {
        color: "white"
    }

    static readonly SELECTION_STYLE = {
        color: "blue"
    }

}


