import { NodeDto } from "../model/node-dto";
import { DistanceCalculator } from "./distance-calculator";
import { List } from "./list";
import { Roads } from "./roads";

export class Nodes {

    static getNodeFromID(id: number, nodes: NodeDto[]): NodeDto {
        let node: NodeDto;
        nodes.forEach(n => {
            if (n.Id == id) {
                node = n;
            }
        });
        return node;
    }

    static getCoordinates(id: number, nodes: NodeDto[]): Array<any> {
        let coord = new Array();
        for (let i = 0; i < nodes.length; i++) {
            if (nodes[i].Id === id) {
                coord = ([nodes[i].Latitude,nodes[i].Longitude]);
            }
        }
        return coord;
    }

    static nearestNode(lat: number, long: number, nodes: NodeDto[]) {
        let node: NodeDto;
        let minDist = Number.POSITIVE_INFINITY;
        nodes.forEach(n => {
            let dist = DistanceCalculator.getDistanceNodeToNode(lat, long, n.Latitude, n.Longitude);
            if (dist < minDist) {
                minDist = dist;
                node = n;
            }
        });
        return node;
    }

}