import { PoiDto } from "../model/poi-dto";
import { Map } from "./map";

//test non implementato perchè per ora non viene utilizzata
export class Pois {
    static searchPoiFromName(s: string, poi: Map<PoiDto, L.Marker>): PoiDto {
        let res;
        poi.entries().list().forEach(p => {
            if (p.getX().Name == s) {
                res = p.getX();
            }
        });
        return res;
    }
}