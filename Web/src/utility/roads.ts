import { RoadDto } from "../model/road-dto";
import { Map } from "./map";
import { NodeDto } from "../model/node-dto";
import { IList } from "./i-list";
import { List } from "./list";

export class Roads {
    //Per ora nel programma non viene utilizzata. Controllare
    /*static searchPolyFromStartNode(id: number, map: Map<RoadDto, L.Polyline>): L.Polyline {
        let pol: L.Polyline;
        map.entries().list().forEach(p => {
            if (p.getX().StartNode == id || p.getX().EndNode == id) {
                pol = p.getY();
            }
        });
        return pol;
    }*/

    static getRoadsFromNode(node: NodeDto, roads: RoadDto[]) : IList<RoadDto> { 
        let roadVet: IList<RoadDto> = new List<RoadDto>();
        roads.forEach(r => {
            if(r.StartNode === node.Id || r.EndNode === node.Id) {
                roadVet.add(r);
            }
        });
        return roadVet;
    }

    static getRoadBetweenNodes(n1: NodeDto, n2: NodeDto, roads: RoadDto[]): RoadDto {
        let road: RoadDto;
        this.getRoadsFromNode(n1, roads).list().forEach(r => {
            if (r.StartNode === n2.Id || r.EndNode === n2.Id) {
                road = r;
            }
        });
        return road;
    }
}