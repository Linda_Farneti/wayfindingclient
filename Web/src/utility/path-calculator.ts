import { RoadDto } from "../model/road-dto";
import { IList } from "./i-list";
import { NodeDto } from "../model/node-dto";
import { List } from "./list";
import { ArrayMap } from "./array-map";
import { Map } from "./map";
import { Nodes } from "./nodes";
import { Roads } from "./roads";

export class PathCalculator {

    static getPath(startNode: number, endNode: number, nodes: NodeDto[], roads: RoadDto[]) : Array<RoadDto> {

        let s: IList<NodeDto> = new List<NodeDto>();
        let t: IList<NodeDto> = new List<NodeDto>();
        let f: Map<NodeDto, number> = new ArrayMap<NodeDto,number>();
        let j: Map<NodeDto, NodeDto> = new ArrayMap<NodeDto, NodeDto>(); 

        let neighbors: IList<NodeDto> = new List<NodeDto>();
        let distance:number;
        let nearest: NodeDto;
 
        nodes.forEach(n => {
            f.push(n, Number.POSITIVE_INFINITY);
            j.push(n, undefined);
        });

        let tempStartNode = Nodes.getNodeFromID(startNode, nodes);
        let tempEndNode = Nodes.getNodeFromID(endNode, nodes);
 
        f.replace(tempStartNode, tempStartNode, 0);
        this.getNeighbour(tempStartNode, nodes, roads).list().forEach(n => {
            f.replace(n, n, Roads.getRoadBetweenNodes(tempStartNode, n, roads).Length);
        });
        t.addAll(nodes);
        t.remove(tempStartNode);

        while(!t.isEmpty()){
            nearest = this.getNearest(f,t);
            if (this.getNeighbour(tempStartNode, nodes, roads).contains(nearest)) {
                j.replace(nearest, nearest, tempStartNode);
            }
            t.remove(nearest);
            s.add(nearest);
            neighbors = this.getNeighbour(nearest, nodes, roads);
            neighbors.removeAll(s.list());
            neighbors.list().forEach(n => {
                distance = f.getSecond(nearest) + Roads.getRoadBetweenNodes(nearest, n, roads).Length;
                if(distance < f.getSecond(n)){
                    f.replace(n, n, distance);
                    j.replace(n, n, nearest);
                }
            });
        }

        let pathFound: IList<RoadDto> = new List<RoadDto>();
        let next = tempEndNode;
        let prev;
        do {
            prev = j.getSecond(next);
            pathFound.add(Roads.getRoadBetweenNodes(next, prev, roads));
            next = prev;
         } while (next != tempStartNode);
    
        let path: Array<RoadDto>;
        path = pathFound.list();
        return path;
     }

    static getNeighbour(node: NodeDto, nodes: NodeDto[], roads: RoadDto[]): List<NodeDto> {
        let neighbors = new List<NodeDto>();
        Roads.getRoadsFromNode(node, roads).list().forEach(r => {
            neighbors.add(r.StartNode === node.Id ? Nodes.getNodeFromID(r.EndNode, nodes) : Nodes.getNodeFromID(r.StartNode, nodes));
        });
        return neighbors;
    }

    private static getNearest(map: Map<NodeDto,Number>, t:IList<NodeDto>) : NodeDto {
        let n: NodeDto;
        let distance = Number.POSITIVE_INFINITY;
        t.list().forEach(p => {
            if(map.getSecond(p) < distance) {
                n = p;
                distance = <number>map.getSecond(p);
            }
        });
        return n;
    } 
}