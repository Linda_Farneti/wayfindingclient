export interface Pair<T,K>{
    getX() : T;
    getY() : K;
    equals(x1:T , y1:K) : boolean;
}