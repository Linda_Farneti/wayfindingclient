import { Container } from "inversify";
import TYPES from "./types";
import { IApiClient } from "../controller/i-api-client";
import { MockApiClient } from "../controller/mock-api-client";
import { Controller } from "../controller/controller";
import { CommandObserver } from "../controller/command-observer";
import { MapObserver } from "../controller/map-observer";
import { GpsObserver } from "../controller/gps-observer";
import { GpsObservable } from "../controller/gps-observable";
import { MapDrawer } from "../view/map-drawer";
import { Drawer } from "../view/drawer";
import { BrowserObservable } from "../controller/browser-observable";
import { IBrowserObservable } from "../controller/i-browser-observable";

var container = new Container();
container.bind<IApiClient>(TYPES.IApiClient).to(MockApiClient);
container.bind<CommandObserver>(TYPES.CommandObserver).to(Controller);
container.bind<MapObserver>(TYPES.MapObserver).to(Controller);
container.bind<Drawer>(TYPES.Drawer).to(MapDrawer);
container.bind<GpsObserver>(TYPES.GpsObserver).to(GpsObservable).inSingletonScope();
container.bind<IBrowserObservable>(TYPES.BrowserObservable).to(BrowserObservable).inSingletonScope();

export default container;