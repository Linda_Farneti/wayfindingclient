let TYPES = {
    IApiClient: Symbol("IApiClient"),
    CommandObserver: Symbol("CommandObserver"),
    MapObserver: Symbol("MapObserver"),
    GpsObserver: Symbol("GpsObserver"),
    Drawer: Symbol("Drawer"),
    BrowserObservable: Symbol("BrowserObservable")
};

export default TYPES;