import { Pair } from "./pair";


export class PairImpl<T, K> implements Pair<T,K>{

    private first:T;
    private second:K;

    public constructor(f:T, s:K){
        this.first = f;
        this.second = s;
    }

    public getX() : T{
        return this.first;
    }

    public getY() : K{
        return this.second;
    }

    public equals(x1:T, y1:K) : boolean{
        return this.first === x1 && this.second === y1;
    }
}