import { Pair } from "./pair";
import { PairImpl } from "./pair-impl";
import { Map } from "./map";
import { IList } from "./i-list";
import { List } from "./list";

export class ArrayMap<T,K> implements Map<T,K>{
    private map: IList<Pair<T, K>>;

    public constructor(){
        this.map = new List<Pair<T,K>>();
    }

    public keys() : IList<T>{
        let ar = new List<T>();
        this.map.list().forEach(x => ar.add(x.getX()));
        return ar;
    }
    
    public push(first:T, second:K){
        this.map.list().push(new PairImpl<T,K>(first,second));
    }

    public getFirst(e: K): T{
        let t;
        for(let i = 0; i < this.map.list().length; i++){
            if(this.map.get(i).getY() === e){
                t = this.map.get(i).getX();
            }
        }
        return t;
    }

    public getSecond(e : T): K{
        let t;
        for(let i = 0; i < this.map.list().length; i++){
            if(this.map.get(i).getX() === e){
                t = this.map.get(i).getY();
            }
        }
        return t;
    }

    public entries(): IList<Pair<T, K>> {
        return this.map;
    }

    public replace(oldKey:T, newKey:T, newValue:K) : void{
        this.map.list().forEach(p => {
            if(p.getX() === oldKey){
                this.map.remove(p);
            }
        });
        this.map.add(new PairImpl<T,K>(newKey,newValue));
    }
}