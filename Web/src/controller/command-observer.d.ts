import { Drawer } from "../view/drawer";
import { IApiClient } from "./i-api-client";
import { Pair } from "../utility/pair";

export interface CommandObserver {
    onSearchPressed(id: string): Array<Pair<string, string>>;
    onStartPressed(): void;
    onOptionClicked(s: string): void;
    findPath(id1: number, id2: number) : void
    setDrawer(d: Drawer): void;
    setApiClient(a: IApiClient): void;
    viewAll(): void;
    gpsIndicatorOn(): void;
    gpsIndicatorOff(): void;
}