import { IApiClient } from "./i-api-client";
import { NodeDto } from "../model/node-dto";
import { RoadDto } from "../model/road-dto";
import { PoiDto } from "../model/poi-dto";
import { MapDto } from "../model/map-dto";
import { Consts } from "../utility/consts";
import { injectable} from "inversify";
import "reflect-metadata";

@injectable()
export class HttpApiClient implements IApiClient {

    private path: string;
    private id: number;

    public constructor() {
        this.path = "http://" + Consts.IP_SERVER + "/Wayfinding/";
        this.id = Consts.MAP_ID;
    }

    public getMap(callback:(data: MapDto) => void): void {
        $.ajax({
            url: this.path + "Api/Map/GetMapFromID?ID=" + this.id,
            data: null,
            success: (d)=>{
                callback(d);
            },
            dataType: "json"
        })
    }

    public getNodes(callback:(data: NodeDto[]) => void): void {
        $.ajax({
            url: this.path + "Api/Node/GetNodes?MapID=" + this.id,
            data: null,
            success: (d)=>{
                callback(d);
            },
            dataType: "json"
        })
    }

    public getRoads(callback:(data: RoadDto[]) => void): void {
        let data: any[];
        $.ajax({
            url: this.path + "Api/Road/GetRoads?MapID=" + this.id,
            data: null,
            success: (d)=>{
                callback(d);
            },
            dataType: "json"
        })
    }

    public getPoi(callback:(data: PoiDto[]) => void): void {
        let dataPoi: any[];
        $.ajax({
            url: this.path + "Api/Point/GetPoints?MapID=" + this.id,
            data: null,
            success: (d)=>{
                dataPoi = this.parsingPoi(d);
                callback(d);
            },
            dataType: "json"
        })
    }

    public getPath(startNode: number, endNode: number, callback:(d: Array<RoadDto>) => void): void {
        let myUrl = this.path + "Api/Road/GetPath?FirstNode=" + startNode + "&SecondNode=" + endNode + "&MapID=" + this.id;
        $.ajax({
            type: "get",
            url: myUrl,
            data: null,//JSON.stringify({FirstNode:startNode,SecondNode:endNode})
            success: (d)=>{
                callback(d);
                //this.controller.findPath(data);
            },
            dataType: "json"
        })
    }

    //da eliminare oppure da mettere nel diagramma
    private parsingPoi(d: any[]): any[] {
        for(let i = 0; i < d.length; i++) {
            let poi = d[i];
           /* var p = $('</br><button>').attr({
                type: "button",
                class: "points",
                name: poi.Name,
                id: poi.NodeID
            }).text(poi.Name).click(() => {
                var pos: Array<number> = controller.onPoiPressed(parseInt(poi.NodeID));
                $.ajax ({
                    type: "get",
                    url: this.path + "/Road/GetPath", //?FirstNode=12&SecondNode=34",
                    contentType: "application/json",
                    data: JSON.stringify({FirstNode:pos[0],SecondNode:pos[1]}),
                    success: (i)=> {
                        d = i;
                        controller.findPath(d);
                        $('.details').attr({
                            hidden: false,
                        }).text(poi.Description);
                    },
                    dataType: "json"
                })
            });
            $('#poiList').append(p);
            */
        }    
        return d;
    }
}