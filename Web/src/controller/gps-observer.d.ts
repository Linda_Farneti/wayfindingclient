import { Drawer } from "../view/drawer";
import { Pair } from "../utility/pair";

export interface GpsObserver {
    addObserver(d: Drawer): void;
    //onStart(): void;
    onCoordinatesChanged(lat: number, lon: number): void;
    getActualPosition(): Pair<number, number>;
}