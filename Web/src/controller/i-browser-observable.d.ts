import { CommandObserver } from "./command-observer";
import { IList } from "../utility/i-list";

export interface IBrowserObservable {
    setup(): void;
    addObserver(obs: CommandObserver): void;
    getObservers(): IList<CommandObserver>;
    setMap(map: L.Map): void;
    loadingScreenOn(): void;
    loadingScreenOff(): void;
    openDescriptionPanel(title: string, description: string): void;
}