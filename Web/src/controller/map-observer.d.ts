import { PoiDto } from "../model/poi-dto";
import { RoadDto } from "../model/road-dto";
import { NodeDto } from "../model/node-dto";
import { Pair } from "../utility/pair";

export interface MapObserver {
    onPoiClicked(p: Pair<NodeDto, PoiDto>): void;
}