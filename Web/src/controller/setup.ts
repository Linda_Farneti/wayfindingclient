import * as L from "leaflet";
import { Drawer } from "../view/drawer";
import { MapDrawer } from "../view/map-drawer";
import { Controller } from "./controller";
import { IApiClient } from "./i-api-client";
import { NodeDto } from "../model/node-dto";
import { RoadDto } from "../model/road-dto";
import { PoiDto } from "../model/poi-dto";
import { GpsObserver } from "./gps-observer";
import { MapDto } from "../model/map-dto";
import container from "../utility/inversify.config";
import TYPES from "../utility/types";
import { IBrowserObservable } from "./i-browser-observable";


class Setup {
    
    private map: L.Map;
    private _controller: Controller;
    private _apiClient: IApiClient;
    private _drawer: Drawer;
    private _gps: GpsObserver;

    public setup(): void {
        let _b = container.get<IBrowserObservable>(TYPES.BrowserObservable);
        this._gps = container.get<GpsObserver>(TYPES.GpsObserver);
        _b.loadingScreenOn();

        this._controller = container.get<Controller>(TYPES.CommandObserver);
        this._apiClient = container.get<IApiClient>(TYPES.IApiClient);

        this._apiClient.getMap((m: MapDto) => {
            this.map = L.map('map').setView([m.Latitude, m.Longitude], m.Zoom);
            this._apiClient.getNodes((n: NodeDto[]) => {
                this._apiClient.getRoads((r: RoadDto[]) => {
                    this._apiClient.getPoi((p: PoiDto[]) => {
                        this._drawer = new MapDrawer(this.map, new Array<Array<any>>(n, r, p));
                        this._controller.setDrawer(this._drawer);
                        this._controller.setApiClient(this._apiClient);
                        this._drawer.addObserver(this._controller);
                        _b.addObserver(this._controller);
                        _b.setMap(this.map);
                        _b.setup();
                        this._gps.addObserver(this._drawer);
                        _b.loadingScreenOff();
                    })
                })
            });
        })
        
    }
}

$(document).ready(() => {
    new Setup().setup();
});