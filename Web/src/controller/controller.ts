import { CommandObserver } from "./command-observer";
import { Drawer } from "../view/drawer";
import { MapObserver } from "./map-observer";
import { PoiDto } from "../model/poi-dto";
import { IApiClient } from "./i-api-client";
import { RoadDto } from "../model/road-dto";
import { GpsObserver } from "./gps-observer";
import { Pair } from "../utility/pair";
import { injectable, inject } from "inversify";
import "reflect-metadata";
import TYPES from "../utility/types";
import container from "../utility/inversify.config";
import { IBrowserObservable } from "./i-browser-observable";
import { NodeDto } from "../model/node-dto";

@injectable()
export class Controller implements CommandObserver, MapObserver {

    private drawer: Drawer;
    private apiClient: IApiClient;
    private _gps: GpsObserver;

    public constructor (@inject(TYPES.GpsObserver) _gpsObs: GpsObserver) {
        this._gps = _gpsObs;
    }

    public setDrawer(d: Drawer) {
        this.drawer = d;
    }

    public setApiClient(a: IApiClient) {
        this.apiClient = a;
    }

    public onSearchPressed(id: string): Array<Pair<string, string>> {
        let arr = this.drawer.hidePoi(id);
        return arr;
    }

    public onStartPressed() {
        this.drawer.positioningOn();
        //this._gps.onStart();
    }
    
    public onPoiClicked(p: Pair<NodeDto, PoiDto>) : void {
        let _b = container.get<IBrowserObservable>(TYPES.BrowserObservable);
        _b.openDescriptionPanel(p.getY().Name, p.getY().Description);
        this.findPath(p.getX().Id, p.getY().NodeID);
    }

    public findPath(id1: number, id2: number) : void {
        this.apiClient.getPath(id1, id2, (r) => { 
            this.drawer.findPath(r);
        })
    }

    public viewAll() {
        this.drawer.viewAll();
    }

    public onOptionClicked(s: string) {
        this.drawer.optionClicked(s);
    }

    public gpsIndicatorOn() {
        this.drawer.positioningOn();
    }

    public gpsIndicatorOff() {
        this.drawer.positioningOff();
    }
}