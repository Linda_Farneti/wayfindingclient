import { Drawer } from "../view/drawer";
import { GpsObserver } from "./gps-observer";
import { PairImpl } from "../utility/pair-impl";
import { Pair } from "../utility/pair";
import { injectable } from "inversify";
import "reflect-metadata";
import { IList } from "../utility/i-list";
import { List } from "../utility/list";


@injectable()
export class GpsObservable implements GpsObserver {
    
    private latitude: number = 44.401678;
    private longitude: number = 11.509437;
    private observers: IList<Drawer>;
    
    public constructor() {
        this.observers = new List<Drawer>();
    }


    public addObserver(d: Drawer) {
        this.observers.add(d);
        this.observers.list().forEach(obs => {
            obs.setCoordinates(this.latitude, this.longitude);
        });
    }

    /*public onStart() {
        //(Sicura che questo metodo non vada eliminato?! Eventualmente toglilo anche dai diagrammi oppure parlane)
        //temporaneamente passo come mia posizione iniziale quella del primo nodo memorizzato
        //Questa cosa è da correggere anche nella funzione getActualPosition
        this.observers.list().forEach(obs => {
            obs.onPositionUpdate(this.latitude, this.longitude);
        });
    }*/

    public onCoordinatesChanged(lat: number, lon: number): void {
        this.latitude = lat;
        this.longitude = lon;
        this.observers.list().forEach(obs => {
            obs.onPositionUpdate(this.latitude, this.longitude);
        });
    }

    public getActualPosition(): Pair<number, number> {
        //return new PairImpl<number, number>(this.latitude, this.longitude);
        return new PairImpl<number, number>(44.401678,11.509437);
        //return new PairImpl<number, number>(44.23248291, 12.0079727172852); (coordinate che servivano a Giulio)
    }

   
}