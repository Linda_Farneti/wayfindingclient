import * as L from "leaflet";
import { CommandObserver } from "./command-observer";
import { IList } from "../utility/i-list";
import { List } from "../utility/list";
import * as $ from 'jquery';
import { injectable } from "inversify";
import "reflect-metadata";
import { IBrowserObservable } from "./i-browser-observable";

@injectable()
export class BrowserObservable implements IBrowserObservable {

    private observers: IList<CommandObserver>;
    private map: L.Map;
    private isOpened: boolean;

    public constructor() {
        this.observers = new List<CommandObserver>();
        this.isOpened = false;
    }
    
    public setup() {

        $(".btn-start").click(() => {
            this.getObservers().list().forEach(obs => {
                obs.onStartPressed();
            });
            $(".btn-close").click();
        })

        $(".btn-search").click(()=> {
            if ($(".search").val() != null && $(".search").val() != undefined) {
                let result;
                $(".result-list").empty();
                $(".result-list").show();
                $(".search").css("border-radius", "0");
                this.getObservers().list().forEach(obs => {
                    result = obs.onSearchPressed($(".search").val() as string);
                    result.forEach(r => {
                        var b = $('<input/>').attr({
                            type: 'button',
                            class: 'btn-result',
                            name: r.getX(),
                            value: r.getX() + r.getY()
                        });
                        $(b).on("click", () => {
                            $(".search").val(r.getX());
                            $(".search").css("border-radius", "35px");
                            $(".result-list").hide();
                            this.getObservers().list().forEach(obs => {
                                obs.onOptionClicked(r.getX());
                            });
                        })
                        $(".result-list").append(b);
                    });
                });
            } else {
                $(".result-list").hide();
            }
        });
/*
        $(".result-list").change(() => {
            $(".search").val($(".result-list:selected").val() as string)
            $(".search").css("border-radius", "35px");
            $(".result-list").hide();
            this.getObservers().list().forEach(obs => {
                obs.onOptionClicked($(".result-list:selected").val() as string);
            });
        });  
   */     
        $(".gps-indicator").focus(() => {
            this.getObservers().list().forEach(obs => {
                obs.gpsIndicatorOn();
            });
        });

        $(".gps-indicator").blur(() => {
            this.getObservers().list().forEach(obs => {
                obs.gpsIndicatorOff();
            });
        });

        $(".btn-close").click(() => {
            $(".btn-open").click();
            this.isOpened = false;
            $(".gps-indicator").show();
        });
    }

    public addObserver(obs: CommandObserver){
        this.observers.add(obs);
    }

    public getObservers() : IList<CommandObserver>{
        return this.observers;
    }

    public setMap(map: L.Map) {
        this.map = map;

        //per vedere le coordinate cliccate (fake gps)
        /*(this.map).on("click", (e:any) => {
            console.log("Click on: ", e.latlng);
        });*/
    }

    public loadingScreenOn() {
        document.getElementById("loading-screen").style.display = 'block';
    }

    public loadingScreenOff() {
        document.getElementById("loading-screen").style.display = 'none';
    }

    public openDescriptionPanel(title: string, description: string) {
        if (!this.isOpened) {
            $(".btn-open").click();
            this.isOpened = true;
        }
        $(".gps-indicator").hide();
        $(".description h1").text(title);
        $(".description p").text(description);
    }
}