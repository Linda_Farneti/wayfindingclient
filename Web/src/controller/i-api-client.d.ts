import { CommandObserver } from "./command-observer";
import { NodeDto } from "../model/node-dto";
import { RoadDto } from "../model/road-dto";
import { PoiDto } from "../model/poi-dto";
import { MapDto } from "../model/map-dto";

export interface IApiClient {
    getNodes(callback:(data: NodeDto[]) => void): void;
    getRoads(callback:(data: RoadDto[]) => void): void;
    getPoi(callback:(data: PoiDto[]) => void): void;
    getPath(startNode: number, endNode: number, callback:(d: Array<RoadDto>) => void): void;
    getMap(callback:(data: MapDto) => void): void;
}