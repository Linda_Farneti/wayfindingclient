export interface PoiDto {
    Name: string;
    NodeID: number;
    Size: number;
    Description: string;
    Icon: string;
}