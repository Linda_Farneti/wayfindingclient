export interface RoadDto {
    ID: number;
    Length: number;
    StartNode: number;
    EndNode: number; 
    Size: number;
}