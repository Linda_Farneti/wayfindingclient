export interface NodeDto {
    Id: number;
    Latitude: number;
    Longitude: number;
}