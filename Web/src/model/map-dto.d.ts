export interface MapDto {
    Id: number;
    Latitude: number;
    Longitude: number;
    Zoom: number;
}