"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var distance_calculator_1 = require("../utility/distance-calculator");
var chai_1 = require("chai");
var nodes_1 = require("../utility/nodes");
var nodes = [{ "Id": 0, "Latitude": 44.402038, "Longitude": 11.509791 },
    { "Id": 1, "Latitude": 44.401678, "Longitude": 11.509437 },
    { "Id": 2, "Latitude": 44.401299, "Longitude": 11.509464 },
    { "Id": 3, "Latitude": 44.400713, "Longitude": 11.508881 }];
describe('getDistanceNodeToNode function', function () {
    it('should return 18617 (distance between Forli and Cesena)', function () {
        var result = distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(44.22255, 12.04100, 44.13722, 12.24187);
        chai_1.expect(Math.round(result)).to.equal(18617);
    });
});
describe('getDistanceNodeToNode function', function () {
    it('should return 0', function () {
        var result = distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(44.22255, 12.04100, 44.22255, 12.04100);
        chai_1.expect(Math.round(result)).to.equal(0);
    });
});
describe('getNodeFromID not found function', function () {
    it('should return error', function () {
        var result = nodes_1.Nodes.getNodeFromID(5, nodes);
        chai_1.expect(result).to.not.exist;
    });
});
describe('getNodeFromID function', function () {
    it('should return error', function () {
        var result = nodes_1.Nodes.getNodeFromID(2, nodes);
        chai_1.expect(result).equal(nodes[2]);
    });
});
describe('nearestNode function', function () {
    it('should return the node 2', function () {
        var result = nodes_1.Nodes.nearestNode(44.401298, 11.509465, nodes);
        chai_1.expect(result).to.deep.equal(nodes_1.Nodes.getNodeFromID(2, nodes));
    });
});
describe('getCoordinates not found function', function () {
    it('should return error', function () {
        var result = nodes_1.Nodes.getCoordinates(8, nodes);
        chai_1.expect(result).to.deep.equal(new Array());
    });
});
describe('getCoordinates function', function () {
    it('should return {44.400713,11.508881}', function () {
        var result = nodes_1.Nodes.getCoordinates(3, nodes);
        chai_1.expect(result).to.deep.equal(new Array(44.400713, 11.508881));
    });
});
