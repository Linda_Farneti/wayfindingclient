"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("mocha");
var chai_1 = require("chai");
var path_calculator_1 = require("../utility/path-calculator");
var list_1 = require("../utility/list");
var nodes = [{ "Id": 0, "Latitude": 44.402038, "Longitude": 11.509791 },
    { "Id": 1, "Latitude": 44.401678, "Longitude": 11.509437 },
    { "Id": 2, "Latitude": 44.401299, "Longitude": 11.509464 },
    { "Id": 3, "Latitude": 44.400713, "Longitude": 11.508881 },
    { "Id": 4, "Latitude": 44.400495, "Longitude": 11.508485 }];
var roads = [{ "ID": 0, "Length": 48.921489976241155, "StartNode": 0, "EndNode": 1, "Size": 10 },
    { "ID": 1, "Length": 42.19742929529782, "StartNode": 1, "EndNode": 2, "Size": 10 },
    { "ID": 2, "Length": 79.94393896295004, "StartNode": 2, "EndNode": 3, "Size": 10 },
    { "ID": 3, "Length": 39.7145675309002, "StartNode": 3, "EndNode": 4, "Size": 10 },
    { "ID": 4, "Length": 56.94393896295004, "StartNode": 2, "EndNode": 0, "Size": 10 },
    { "ID": 5, "Length": 32.7156796309002, "StartNode": 0, "EndNode": 3, "Size": 10 }];
describe('getNeighbor function', function () {
    it('should return nodes 1, 2 and 3', function () {
        var result = path_calculator_1.PathCalculator.getNeighbour(nodes[0], nodes, roads);
        chai_1.expect(result).to.deep.equal(new list_1.List(new Array(nodes[1], nodes[2], nodes[3])));
    });
});
describe('getNeighbor not found function', function () {
    it('should return nodes an empty list', function () {
        var result = path_calculator_1.PathCalculator.getNeighbour({ Id: 8, Latitude: 45.89756, Longitude: 34.467463 }, nodes, roads);
        chai_1.expect(result).to.deep.equal(new list_1.List());
    });
});
//Il testing delle altre funzioni richiede un sacco di lavoro. Sicuro che vada fatto?
