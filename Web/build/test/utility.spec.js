"use strict";
exports.__esModule = true;
var distance_calculator_1 = require("../utility/distance-calculator");
var chai_1 = require("chai");
require("mocha");
var nodes_1 = require("../utility/nodes");
describe('GetDistanceNodeToNode function', function () {
    it('should return 18617 (distance between Forli and Cesena)', function () {
        var result = distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(44.22255, 12.04100, 44.13722, 12.24187);
        chai_1.expect(Math.round(result)).to.equal(18617);
    });
});
describe('searchNodeFromId function', function () {
    var nodes;
    nodes = [{ "Id": 1, "Latitude": 44.401678, "Longitude": 11.509437 }, { "Id": 2, "Latitude": 44.401299, "Longitude": 11.509464 }, { "Id": 3, "Latitude": 44.400713, "Longitude": 11.508881 }];
    it('should return the node {"Id":2,"Latitude":44.401299,"Longitude":11.509464}', function () {
        var result = nodes_1.Nodes.searchNodeFromId(2, nodes);
        chai_1.expect(result).to.equal({ "Id": 2, "Latitude": 44.401299, "Longitude": 11.509464 });
    });
});
