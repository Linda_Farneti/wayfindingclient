"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var pair_impl_1 = require("../utility/pair-impl");
var inversify_1 = require("inversify");
require("reflect-metadata");
var list_1 = require("../utility/list");
var GpsObservable = /** @class */ (function () {
    function GpsObservable() {
        this.latitude = 44.401678;
        this.longitude = 11.509437;
        this.observers = new list_1.List();
    }
    GpsObservable.prototype.addObserver = function (d) {
        var _this = this;
        this.observers.add(d);
        this.observers.list().forEach(function (obs) {
            obs.setCoordinates(_this.latitude, _this.longitude);
        });
    };
    /*public onStart() {
        //(Sicura che questo metodo non vada eliminato?! Eventualmente toglilo anche dai diagrammi oppure parlane)
        //temporaneamente passo come mia posizione iniziale quella del primo nodo memorizzato
        //Questa cosa è da correggere anche nella funzione getActualPosition
        this.observers.list().forEach(obs => {
            obs.onPositionUpdate(this.latitude, this.longitude);
        });
    }*/
    GpsObservable.prototype.onCoordinatesChanged = function (lat, lon) {
        var _this = this;
        this.latitude = lat;
        this.longitude = lon;
        this.observers.list().forEach(function (obs) {
            obs.onPositionUpdate(_this.latitude, _this.longitude);
        });
    };
    GpsObservable.prototype.getActualPosition = function () {
        //return new PairImpl<number, number>(this.latitude, this.longitude);
        return new pair_impl_1.PairImpl(44.401678, 11.509437);
        //return new PairImpl<number, number>(44.23248291, 12.0079727172852); (coordinate che servivano a Giulio)
    };
    GpsObservable = __decorate([
        inversify_1.injectable(),
        __metadata("design:paramtypes", [])
    ], GpsObservable);
    return GpsObservable;
}());
exports.GpsObservable = GpsObservable;
