"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var inversify_1 = require("inversify");
require("reflect-metadata");
var types_1 = require("../utility/types");
var inversify_config_1 = require("../utility/inversify.config");
var Controller = /** @class */ (function () {
    function Controller(_gpsObs) {
        this._gps = _gpsObs;
    }
    Controller.prototype.setDrawer = function (d) {
        this.drawer = d;
    };
    Controller.prototype.setApiClient = function (a) {
        this.apiClient = a;
    };
    Controller.prototype.onSearchPressed = function (id) {
        var arr = this.drawer.hidePoi(id);
        return arr;
    };
    Controller.prototype.onStartPressed = function () {
        this.drawer.positioningOn();
        //this._gps.onStart();
    };
    Controller.prototype.onPoiClicked = function (p) {
        var _b = inversify_config_1.default.get(types_1.default.BrowserObservable);
        _b.openDescriptionPanel(p.getY().Name, p.getY().Description);
        this.findPath(p.getX().Id, p.getY().NodeID);
    };
    Controller.prototype.findPath = function (id1, id2) {
        var _this = this;
        this.apiClient.getPath(id1, id2, function (r) {
            _this.drawer.findPath(r);
        });
    };
    Controller.prototype.viewAll = function () {
        this.drawer.viewAll();
    };
    Controller.prototype.onOptionClicked = function (s) {
        this.drawer.optionClicked(s);
    };
    Controller.prototype.gpsIndicatorOn = function () {
        this.drawer.positioningOn();
    };
    Controller.prototype.gpsIndicatorOff = function () {
        this.drawer.positioningOff();
    };
    Controller = __decorate([
        inversify_1.injectable(),
        __param(0, inversify_1.inject(types_1.default.GpsObserver)),
        __metadata("design:paramtypes", [Object])
    ], Controller);
    return Controller;
}());
exports.Controller = Controller;
