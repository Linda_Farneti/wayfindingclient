"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var L = require("leaflet");
var map_drawer_1 = require("../view/map-drawer");
var inversify_config_1 = require("../utility/inversify.config");
var types_1 = require("../utility/types");
var Setup = /** @class */ (function () {
    function Setup() {
    }
    Setup.prototype.setup = function () {
        var _this = this;
        var _b = inversify_config_1.default.get(types_1.default.BrowserObservable);
        this._gps = inversify_config_1.default.get(types_1.default.GpsObserver);
        _b.loadingScreenOn();
        this._controller = inversify_config_1.default.get(types_1.default.CommandObserver);
        this._apiClient = inversify_config_1.default.get(types_1.default.IApiClient);
        this._apiClient.getMap(function (m) {
            _this.map = L.map('map').setView([m.Latitude, m.Longitude], m.Zoom);
            _this._apiClient.getNodes(function (n) {
                _this._apiClient.getRoads(function (r) {
                    _this._apiClient.getPoi(function (p) {
                        _this._drawer = new map_drawer_1.MapDrawer(_this.map, new Array(n, r, p));
                        _this._controller.setDrawer(_this._drawer);
                        _this._controller.setApiClient(_this._apiClient);
                        _this._drawer.addObserver(_this._controller);
                        _b.addObserver(_this._controller);
                        _b.setMap(_this.map);
                        _b.setup();
                        _this._gps.addObserver(_this._drawer);
                        _b.loadingScreenOff();
                    });
                });
            });
        });
    };
    return Setup;
}());
$(document).ready(function () {
    new Setup().setup();
});
