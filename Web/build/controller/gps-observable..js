"use strict";
exports.__esModule = true;
var GpsObservable = /** @class */ (function () {
    function GpsObservable() {
    }
    GpsObservable.prototype.setup = function () {
        //prendere le coordinate gps
    };
    GpsObservable.prototype.onStart = function () {
        //mandare l'update sulle nuove coordinate
        this.observer.onPositionUpdate(1234, 4357);
    };
    GpsObservable.prototype.setDrawer = function (d) {
        this.observer = d;
    };
    return GpsObservable;
}());
exports.GpsObservable = GpsObservable;
