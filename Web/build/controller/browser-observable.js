"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var list_1 = require("../utility/list");
var $ = require("jquery");
var inversify_1 = require("inversify");
require("reflect-metadata");
var BrowserObservable = /** @class */ (function () {
    function BrowserObservable() {
        this.observers = new list_1.List();
        this.isOpened = false;
    }
    BrowserObservable.prototype.setup = function () {
        var _this = this;
        $(".btn-start").click(function () {
            _this.getObservers().list().forEach(function (obs) {
                obs.onStartPressed();
            });
            $(".btn-close").click();
        });
        $(".btn-search").click(function () {
            if ($(".search").val() != null && $(".search").val() != undefined) {
                var result_1;
                $(".result-list").empty();
                $(".result-list").show();
                $(".search").css("border-radius", "0");
                _this.getObservers().list().forEach(function (obs) {
                    result_1 = obs.onSearchPressed($(".search").val());
                    result_1.forEach(function (r) {
                        var b = $('<input/>').attr({
                            type: 'button',
                            class: 'btn-result',
                            name: r.getX(),
                            value: r.getX() + r.getY()
                        });
                        $(b).on("click", function () {
                            $(".search").val(r.getX());
                            $(".search").css("border-radius", "35px");
                            $(".result-list").hide();
                            _this.getObservers().list().forEach(function (obs) {
                                obs.onOptionClicked(r.getX());
                            });
                        });
                        $(".result-list").append(b);
                    });
                });
            }
            else {
                $(".result-list").hide();
            }
        });
        /*
                $(".result-list").change(() => {
                    $(".search").val($(".result-list:selected").val() as string)
                    $(".search").css("border-radius", "35px");
                    $(".result-list").hide();
                    this.getObservers().list().forEach(obs => {
                        obs.onOptionClicked($(".result-list:selected").val() as string);
                    });
                });
           */
        $(".gps-indicator").focus(function () {
            _this.getObservers().list().forEach(function (obs) {
                obs.gpsIndicatorOn();
            });
        });
        $(".gps-indicator").blur(function () {
            _this.getObservers().list().forEach(function (obs) {
                obs.gpsIndicatorOff();
            });
        });
        $(".btn-close").click(function () {
            $(".btn-open").click();
            _this.isOpened = false;
            $(".gps-indicator").show();
        });
    };
    BrowserObservable.prototype.addObserver = function (obs) {
        this.observers.add(obs);
    };
    BrowserObservable.prototype.getObservers = function () {
        return this.observers;
    };
    BrowserObservable.prototype.setMap = function (map) {
        this.map = map;
        //per vedere le coordinate cliccate (fake gps)
        /*(this.map).on("click", (e:any) => {
            console.log("Click on: ", e.latlng);
        });*/
    };
    BrowserObservable.prototype.loadingScreenOn = function () {
        document.getElementById("loading-screen").style.display = 'block';
    };
    BrowserObservable.prototype.loadingScreenOff = function () {
        document.getElementById("loading-screen").style.display = 'none';
    };
    BrowserObservable.prototype.openDescriptionPanel = function (title, description) {
        if (!this.isOpened) {
            $(".btn-open").click();
            this.isOpened = true;
        }
        $(".gps-indicator").hide();
        $(".description h1").text(title);
        $(".description p").text(description);
    };
    BrowserObservable = __decorate([
        inversify_1.injectable(),
        __metadata("design:paramtypes", [])
    ], BrowserObservable);
    return BrowserObservable;
}());
exports.BrowserObservable = BrowserObservable;
