"use strict";
exports.__esModule = true;
var list_1 = require("../utility/list");
var IconManager = /** @class */ (function () {
    function IconManager() {
        this.iconsDB = new list_1.List(new Array());
    }
    IconManager.prototype.getIconsDB = function () {
        return this.iconsDB;
    };
    IconManager.getInstance = function () {
        if (this.instance === undefined || this.instance === null) {
            this.instance = new IconManager();
        }
        return this.instance;
    };
    return IconManager;
}());
exports.IconManager = IconManager;
