"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var L = require("leaflet");
var array_map_1 = require("../utility/array-map");
var list_1 = require("../utility/list");
var pair_impl_1 = require("../utility/pair-impl");
var distance_calculator_1 = require("../utility/distance-calculator");
var consts_1 = require("../utility/consts");
var nodes_1 = require("../utility/nodes");
var position_1 = require("../utility/position");
var path_calculator_1 = require("../utility/path-calculator");
var pois_1 = require("../utility/pois");
var inversify_1 = require("inversify");
require("reflect-metadata");
var types_1 = require("../utility/types");
var inversify_config_1 = require("../utility/inversify.config");
var MapDrawer = /** @class */ (function () {
    function MapDrawer(map, data) {
        this.positioning = null;
        this.nodes = new Array();
        this.roads = new array_map_1.ArrayMap();
        this.poi = new array_map_1.ArrayMap();
        this.path = new array_map_1.ArrayMap();
        this.map = map;
        this.drawMap(data);
        this.mapObserver = new list_1.List();
        this._gps = inversify_config_1.default.get(types_1.default.GpsObserver);
        //this.lat = this._gps.getActualPosition().getX();
        //this.lon = this._gps.getActualPosition().getY();
        //let actualPos = this.gps.getActualPosition().getX();
        /*
        let actualLat = this.gps.getActualLatitude();
        $("#utile").text("Lat: " + actualLat);
        let actualLong = this.gps.getActualLongitude();
        this.setNewPosition(actualLat, actualLong);
        */
    }
    MapDrawer.prototype.drawPoi = function (c) {
        var _this = this;
        var coord = Array();
        coord.push(nodes_1.Nodes.getCoordinates(c.NodeID, this.nodes));
        var icon = L.icon({
            //iconUrl: "http://" + Consts.IP_SERVER + "/Wayfinding/Images/" + c.Icon + ".svg",
            iconUrl: "http://" + consts_1.Consts.IP_CLIENT + "/images/" + c.Icon + ".svg",
            iconSize: [c.Size, c.Size]
        });
        var marker = L.marker(coord[0]);
        marker.setIcon(icon);
        marker.addTo(this.map);
        marker.on("click", function () {
            _this.optionClicked(c.Name);
        });
        this.poi.push(c, marker);
    };
    MapDrawer.prototype.drawRoad = function (r) {
        var coord = Array();
        coord.push(nodes_1.Nodes.getCoordinates(r.StartNode, this.nodes));
        coord.push(nodes_1.Nodes.getCoordinates(r.EndNode, this.nodes));
        var newLine = L.polyline(coord, {
            color: "white",
            weight: r.Size
        }).addTo(this.map);
        this.roads.push(r, newLine);
    };
    MapDrawer.prototype.resetPath = function () {
        var _this = this;
        this.path.entries().list().forEach(function (r) {
            _this.map.removeLayer(r.getY());
        });
        this.path = new array_map_1.ArrayMap();
    };
    MapDrawer.prototype.underlineRoad = function (r) {
        var coord = Array();
        coord.push(nodes_1.Nodes.getCoordinates(r.StartNode, this.nodes));
        coord.push(nodes_1.Nodes.getCoordinates(r.EndNode, this.nodes));
        var newLine = L.polyline(coord, {
            color: "green",
            lineJoin: 'round',
            weight: 5,
            opacity: 1,
        }).addTo(this.map);
        this.path.push(r, newLine);
    };
    /*private updatePath(lat: number, long: number) {
        let t: boolean;
        let coord = Array();
        coord.push([lat, long]);
        this.path.entries().list().forEach(p => {
            if (p.getX().StartNode == this.lastCrossed.getX().Id) {
                t = true;
            }
            if (t == true) {
                coord.push(p.getY().getLatLngs()[1]);
            }
        });
        this.hidePath();
        this.lastCrossed.getY().setLatLngs(coord).setStyle({
            color: "green"
        });
        this.lastCrossed.getY().bringToFront();
    } */
    MapDrawer.prototype.hidePath = function () {
        this.path.entries().list().forEach(function (p) {
            p.getY().setStyle({
                color: "white"
            });
        });
    };
    MapDrawer.prototype.setNewPosition = function (x, y) {
        this.lat = x;
        this.lon = y;
        this.moveMarker();
    };
    MapDrawer.prototype.moveMarker = function () {
        if (this.pointSelector != undefined && this.pointSelector != null) {
            this.map.removeLayer(this.pointSelector);
        }
        var pointIcon = L.icon({
            iconUrl: "./images/positionMarker2.png",
            iconSize: [60, 60]
        });
        this.pointSelector = L.marker([this.lat, this.lon]);
        this.pointSelector.setIcon(pointIcon);
        this.pointSelector.addTo(this.map);
        if (this.positioning) {
            this.mapViewUpdate();
        }
    };
    MapDrawer.prototype.setDefaultView = function () {
        this.map.setZoom(16.4);
    };
    MapDrawer.prototype.mapViewUpdate = function () {
        this.map.setView(new L.LatLng(this.lat, this.lon), 19);
        this.map.dragging.enable;
    };
    MapDrawer.prototype.optionClicked = function (s) {
        var _this = this;
        var p = pois_1.Pois.searchPoiFromName(s, this.poi);
        this.mapObserver.list().forEach(function (obs) {
            obs.onPoiClicked(new pair_impl_1.PairImpl(position_1.Position.getPosition(_this.nodes), p));
        });
        this.setDefaultView();
    };
    MapDrawer.prototype.drawMap = function (data) {
        var _this = this;
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            maxZoom: 19
        }).addTo(this.map);
        data[0].forEach(function (n) {
            var newPoint = { Id: n.Id, Latitude: n.Latitude, Longitude: n.Longitude };
            _this.nodes.push(newPoint);
        });
        data[1].forEach(function (r) {
            _this.drawRoad(r);
        });
        data[2].forEach(function (p) {
            _this.drawPoi(p);
        });
    };
    MapDrawer.prototype.findPath = function (data) {
        var _this = this;
        this.resetPath();
        data.forEach(function (n) {
            _this.underlineRoad(n);
        });
        for (var i = 0; data[i] != null; i++) {
            this.underlineRoad(data[i]);
        }
        this.setNewPosition(position_1.Position.getPosition(this.nodes).Latitude, position_1.Position.getPosition(this.nodes).Longitude);
        //this.lastCrossed = new PairImpl(this.nodes[0], this.path.entries().list()[0].getY());
    };
    MapDrawer.prototype.onPositionUpdate = function (x, y) {
        this.setNewPosition(x, y);
        for (var i = 0; i < this.nodes.length; i++) {
            //da sistemare in base all'algoritmo di correzione
            //if (this.nearestNode(x, y) == this.nodes[i] && this.nodes[i] != this.lastCrossed.getX()) {
            if (this.nodes[i].Latitude == x && this.nodes[i].Longitude == y) {
                //this.lastCrossed = new PairImpl(this.nodes[i],this.searchPolyFromStartNode(this.nodes[i].Id));
            }
        }
        //this.updatePath(x, y);
    };
    MapDrawer.prototype.hidePoi = function (id) {
        var _this = this;
        var arr = new Array();
        this.resetPath();
        this.poi.entries().list().forEach(function (p) {
            p.getY().addTo(_this.map);
            //bisogna scegliere come fare le ricerche, per ora risultano anche con singole lettere
            if (p.getX().Name.toLowerCase().indexOf(id.toLowerCase()) < 0) {
                //if (p.getX().Name.substr(0, id.length).toLowerCase() != id) {
                _this.map.removeLayer(p.getY());
            }
            else {
                var tmpPath = path_calculator_1.PathCalculator.getPath(position_1.Position.getPosition(_this.nodes).Id, p.getX().NodeID, _this.nodes, _this.roads.keys().list());
                var dist_1 = 0;
                tmpPath.forEach(function (r) {
                    dist_1 += distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(nodes_1.Nodes.getCoordinates(r.StartNode, _this.nodes)[0], nodes_1.Nodes.getCoordinates(r.StartNode, _this.nodes)[1], nodes_1.Nodes.getCoordinates(r.EndNode, _this.nodes)[0], nodes_1.Nodes.getCoordinates(r.EndNode, _this.nodes)[1]);
                });
                arr.push(new pair_impl_1.PairImpl(p.getX().Name, " (" + Math.round(dist_1) + "m)"));
            }
        });
        return arr;
    };
    MapDrawer.prototype.setCoordinates = function (lat, lon) {
        this.lat = lat;
        this.lon = lon;
    };
    MapDrawer.prototype.viewAll = function () {
        var _this = this;
        this.poi.entries().list().forEach(function (p) {
            p.getY().addTo(_this.map);
        });
    };
    MapDrawer.prototype.addObserver = function (obs) {
        this.mapObserver.add(obs);
    };
    MapDrawer.prototype.positioningOn = function () {
        this.positioning = true;
        this.mapViewUpdate();
    };
    MapDrawer.prototype.positioningOff = function () {
        this.positioning = false;
    };
    MapDrawer = __decorate([
        inversify_1.injectable(),
        __metadata("design:paramtypes", [Object, Array])
    ], MapDrawer);
    return MapDrawer;
}());
exports.MapDrawer = MapDrawer;
