"use strict";
exports.__esModule = true;
var GraphRoad = /** @class */ (function () {
    function GraphRoad(length, startNodeId, endNodeId, size) {
        this.length = length;
        this.startNodeId = startNodeId;
        this.endNodeId = endNodeId;
        this.size = size;
    }
    GraphRoad.prototype.getLength = function () {
        return this.length;
    };
    GraphRoad.prototype.getStartNode = function () {
        return this.startNodeId;
    };
    GraphRoad.prototype.getEndNode = function () {
        return this.endNodeId;
    };
    GraphRoad.prototype.getSize = function () {
        return this.size;
    };
    return GraphRoad;
}());
exports.GraphRoad = GraphRoad;
