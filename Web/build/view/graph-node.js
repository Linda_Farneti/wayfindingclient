"use strict";
exports.__esModule = true;
var GraphNode = /** @class */ (function () {
    function GraphNode(id, lat, long) {
        this.id = id;
        this.latitude = lat;
        this.longitude = long;
    }
    GraphNode.prototype.getId = function () {
        return this.id;
    };
    GraphNode.prototype.setId = function (id) {
        this.id = id;
    };
    GraphNode.prototype.getLatitude = function () {
        return this.latitude;
    };
    GraphNode.prototype.getLongitude = function () {
        return this.longitude;
    };
    GraphNode.prototype.setLatitude = function (lat) {
        this.latitude = lat;
    };
    GraphNode.prototype.setLongitude = function (long) {
        this.longitude = long;
    };
    GraphNode.prototype.toString = function () {
        return this.id + " " + "(" + this.latitude + "," + this.longitude + ")";
    };
    return GraphNode;
}());
exports.GraphNode = GraphNode;
