"use strict";
exports.__esModule = true;
var GraphPointOfInterest = /** @class */ (function () {
    function GraphPointOfInterest(name, nodeId, size, description, icon) {
        this.name = name;
        this.nodeId = nodeId;
        this.size = size;
        this.description = description;
        this.icon = icon;
    }
    GraphPointOfInterest.prototype.getName = function () {
        return this.name;
    };
    GraphPointOfInterest.prototype.getNodeId = function () {
        return this.nodeId;
    };
    GraphPointOfInterest.prototype.getSize = function () {
        return this.size;
    };
    GraphPointOfInterest.prototype.getDescription = function () {
        return this.description;
    };
    GraphPointOfInterest.prototype.getIcon = function () {
        return this.icon;
    };
    return GraphPointOfInterest;
}());
exports.GraphPointOfInterest = GraphPointOfInterest;
