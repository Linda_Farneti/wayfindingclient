"use strict";
exports.__esModule = true;
//da cancellare
var FakeGpsObservable = /** @class */ (function () {
    function FakeGpsObservable() {
    }
    FakeGpsObservable.prototype.latitudeUpd = function (lat) {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.longitudeUpd = function (lon) {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.getActualLongitude = function () {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.getActualLatitude = function () {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.getAtcualLongitude = function () {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.latitudeUpdate = function (lat) {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.longitudeUpdate = function (lon) {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.onCoordinatesChanged = function (lat, lon) {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.getActualPosition = function () {
        throw new Error("Method not implemented.");
    };
    FakeGpsObservable.prototype.setup = function (d) {
        this.observer = d;
        this.path = new Array([44.40204047616258, 11.509755489476516], [44.402002176721304, 11.509648180744817], [44.401952387410155, 11.509524775703351], [44.40197536709747, 11.509369178042396], [44.40202515638907, 11.509186753198485], [44.402067285756544, 11.508998962918], [44.40209792527746, 11.508832634383893], [44.40213622465605, 11.508607286047301], [44.402174524009574, 11.508424861203391], [44.40220133354214, 11.508226340049756], [44.40221665326949, 11.5080922041351], [44.40222431313167, 11.507947337347336], [44.402212823338004, 11.507807835996106]);
    };
    FakeGpsObservable.prototype.onStart = function () {
        var _this = this;
        var i = 0;
        var int = setInterval(function () {
            _this.observer.onPositionUpdate(_this.path[i][0], _this.path[i][1]);
            i++;
            if (i === _this.path.length) {
                clearInterval(int);
            }
        }, 250);
    };
    return FakeGpsObservable;
}());
exports.FakeGpsObservable = FakeGpsObservable;
