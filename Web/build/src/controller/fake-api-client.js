"use strict";
exports.__esModule = true;
var array_map_1 = require("../utility/array-map");
var list_1 = require("../utility/list");
var consts_1 = require("../utility/consts");
var FakeApiClient = /** @class */ (function () {
    function FakeApiClient(c) {
        this.controller = c;
        this.nodes = new Array();
        this.roads = new Array();
        this.poi = new Array();
    }
    FakeApiClient.prototype.getMap = function (callback) {
        var map = {
            Id: 10,
            Latitude: consts_1.Consts.LATITUDE,
            Longitude: consts_1.Consts.LONGITUDE,
            Zoom: consts_1.Consts.MAP_FOCUS
        };
        callback(map);
    };
    FakeApiClient.prototype.getNodes = function (callback) {
        this.nodes = [{ "Id": 0, "Latitude": 44.402038, "Longitude": 11.509791 },
            { "Id": 1, "Latitude": 44.401678, "Longitude": 11.509437 },
            { "Id": 2, "Latitude": 44.401299, "Longitude": 11.509464 },
            { "Id": 3, "Latitude": 44.400713, "Longitude": 11.508881 },
            { "Id": 4, "Latitude": 44.400495, "Longitude": 11.508485 },
            { "Id": 5, "Latitude": 44.401437, "Longitude": 11.506727 },
            { "Id": 6, "Latitude": 44.401783, "Longitude": 11.506288 },
            { "Id": 7, "Latitude": 44.402409, "Longitude": 11.505872 },
            { "Id": 8, "Latitude": 44.403129, "Longitude": 11.505669 },
            { "Id": 9, "Latitude": 44.403462, "Longitude": 11.50583 },
            { "Id": 10, "Latitude": 44.402305, "Longitude": 11.506098 },
            { "Id": 11, "Latitude": 44.401752, "Longitude": 11.506587 },
            { "Id": 12, "Latitude": 44.40085715775432, "Longitude": 11.508454394146167 },
            { "Id": 13, "Latitude": 44.40094950537373, "Longitude": 11.5086795576585 },
            { "Id": 14, "Latitude": 44.4011520324977, "Longitude": 11.508127327024589 },
            { "Id": 15, "Latitude": 44.401542831508706, "Longitude": 11.507864557658458 },
            { "Id": 16, "Latitude": 44.40181878730228, "Longitude": 11.507902639414585 },
            { "Id": 17, "Latitude": 44.401984, "Longitude": 11.507731 },
            { "Id": 18, "Latitude": 44.40222866291907, "Longitude": 11.508101029269097 },
            { "Id": 19, "Latitude": 44.402091, "Longitude": 11.50876 },
            { "Id": 20, "Latitude": 44.401934, "Longitude": 11.509516 },
            { "Id": 21, "Latitude": 44.402242, "Longitude": 11.510018 },
            { "Id": 22, "Latitude": 44.402425, "Longitude": 11.510254 },
            { "Id": 23, "Latitude": 44.402586, "Longitude": 11.510812 },
            { "Id": 24, "Latitude": 44.402778, "Longitude": 11.511246 },
            { "Id": 25, "Latitude": 44.403061, "Longitude": 11.511069 },
            { "Id": 26, "Latitude": 44.402889, "Longitude": 11.510292 },
            { "Id": 27, "Latitude": 44.402705, "Longitude": 11.509804278829249 },
            { "Id": 28, "Latitude": 44.402552, "Longitude": 11.509182 },
            { "Id": 29, "Latitude": 44.402544, "Longitude": 11.508785 },
            { "Id": 30, "Latitude": 44.403522, "Longitude": 11.510773 },
            { "Id": 31, "Latitude": 44.403733, "Longitude": 11.51058 },
            { "Id": 32, "Latitude": 44.403618, "Longitude": 11.510259 },
            { "Id": 33, "Latitude": 44.403135, "Longitude": 11.509465 },
            { "Id": 34, "Latitude": 44.403082, "Longitude": 11.508661 },
            { "Id": 35, "Latitude": 44.40302, "Longitude": 11.509063 },
            { "Id": 36, "Latitude": 44.403377, "Longitude": 11.508211 },
            { "Id": 37, "Latitude": 44.403725, "Longitude": 11.507814 },
            { "Id": 38, "Latitude": 44.403886, "Longitude": 11.507487 },
            { "Id": 39, "Latitude": 44.403959, "Longitude": 11.506313 },
            { "Id": 40, "Latitude": 44.403691, "Longitude": 11.506152 },
            { "Id": 41, "Latitude": 44.403269, "Longitude": 11.506446 },
            { "Id": 42, "Latitude": 44.402652, "Longitude": 11.506762 },
            { "Id": 43, "Latitude": 44.402936, "Longitude": 11.507561 },
            { "Id": 44, "Latitude": 44.402235, "Longitude": 11.507137 },
            { "Id": 45, "Latitude": 44.402695, "Longitude": 11.508209 },
            { "Id": 46, "Latitude": 44.403415, "Longitude": 11.507432 },
            { "Id": 47, "Latitude": 44.403679, "Longitude": 11.506869 },
            { "Id": 48, "Latitude": 44.401221, "Longitude": 11.507458 },
            { "Id": 49, "Latitude": 44.40297, "Longitude": 11.505825 },
            { "Id": 50, "Latitude": 44.40222149437976, "Longitude": 11.507805442341542 }];
        // {"Id":51,"Latitude":44.402036,"Longitude":11.509733}]
        callback(this.nodes);
    };
    FakeApiClient.prototype.getRoads = function (callback) {
        this.roads = [{ "ID": 0, "Length": 36.09337068649179, "StartNode": 9, "EndNode": 40, "Size": 10 },
            { "ID": 1, "Length": 32.428946465747636, "StartNode": 40, "EndNode": 39, "Size": 10 },
            { "ID": 2, "Length": 93.61562093818793, "StartNode": 39, "EndNode": 38, "Size": 10 },
            { "ID": 3, "Length": 31.548427814119233, "StartNode": 38, "EndNode": 37, "Size": 10 },
            { "ID": 4, "Length": 49.92009401091972, "StartNode": 37, "EndNode": 36, "Size": 10 },
            { "ID": 5, "Length": 48.517700131736625, "StartNode": 36, "EndNode": 34, "Size": 10 },
            { "ID": 6, "Length": 32.67117857836385, "StartNode": 34, "EndNode": 35, "Size": 10 },
            { "ID": 7, "Length": 34.40050281493861, "StartNode": 35, "EndNode": 33, "Size": 10 },
            { "ID": 8, "Length": 82.8436743026555, "StartNode": 33, "EndNode": 32, "Size": 10 },
            { "ID": 9, "Length": 28.52704821222271, "StartNode": 32, "EndNode": 31, "Size": 10 },
            { "ID": 10, "Length": 28.027559559314543, "StartNode": 31, "EndNode": 30, "Size": 10 },
            { "ID": 11, "Length": 56.39692384550201, "StartNode": 30, "EndNode": 25, "Size": 10 },
            { "ID": 12, "Length": 34.466832341329166, "StartNode": 25, "EndNode": 24, "Size": 10 },
            { "ID": 13, "Length": 40.55269984150934, "StartNode": 24, "EndNode": 23, "Size": 10 },
            { "ID": 14, "Length": 47.807320233784466, "StartNode": 23, "EndNode": 22, "Size": 10 },
            { "ID": 15, "Length": 27.668983887754706, "StartNode": 22, "EndNode": 21, "Size": 10 },
            { "ID": 16, "Length": 28.97863243429187, "StartNode": 21, "EndNode": 0, "Size": 10 },
            { "ID": 17, "Length": 48.921489976241155, "StartNode": 0, "EndNode": 1, "Size": 10 },
            { "ID": 18, "Length": 42.19742929529782, "StartNode": 1, "EndNode": 2, "Size": 10 },
            { "ID": 19, "Length": 79.94393896295004, "StartNode": 2, "EndNode": 3, "Size": 10 },
            { "ID": 20, "Length": 39.71580196309002, "StartNode": 3, "EndNode": 4, "Size": 10 },
            { "ID": 21, "Length": 174.57801257331585, "StartNode": 4, "EndNode": 5, "Size": 10 },
            { "ID": 22, "Length": 51.92804357838483, "StartNode": 5, "EndNode": 6, "Size": 10 },
            { "ID": 23, "Length": 77.0549372945496, "StartNode": 6, "EndNode": 7, "Size": 10 },
            { "ID": 24, "Length": 81.6684169897084, "StartNode": 7, "EndNode": 8, "Size": 10 },
            { "ID": 25, "Length": 24.718750864017807, "StartNode": 0, "EndNode": 20, "Size": 10 },
            { "ID": 26, "Length": 62.54471005956014, "StartNode": 20, "EndNode": 19, "Size": 10 },
            { "ID": 27, "Length": 90.96300352640117, "StartNode": 11, "EndNode": 48, "Size": 10 },
            { "ID": 28, "Length": 72.73414100970787, "StartNode": 10, "EndNode": 11, "Size": 10 },
            { "ID": 29, "Length": 54.70934585005948, "StartNode": 9, "EndNode": 49, "Size": 10 },
            { "ID": 30, "Length": 77.05948642440093, "StartNode": 49, "EndNode": 10, "Size": 10 },
            { "ID": 31, "Length": 54.54255321087046, "StartNode": 18, "EndNode": 19, "Size": 10 },
            { "ID": 32, "Length": 27.062258414715018, "StartNode": 50, "EndNode": 17, "Size": 10 },
            { "ID": 33, "Length": 23.49575327549125, "StartNode": 50, "EndNode": 18, "Size": 10 },
            { "ID": 34, "Length": 22.878281209962918, "StartNode": 16, "EndNode": 17, "Size": 10 },
            { "ID": 35, "Length": 88.89752095283602, "StartNode": 12, "EndNode": 48, "Size": 10 },
            { "ID": 36, "Length": 20.625823586080685, "StartNode": 12, "EndNode": 13, "Size": 10 },
            { "ID": 37, "Length": 49.31394922959008, "StartNode": 14, "EndNode": 13, "Size": 10 },
            { "ID": 38, "Length": 30.83366406887033, "StartNode": 15, "EndNode": 16, "Size": 10 },
            { "ID": 39, "Length": 48.20901239575655, "StartNode": 15, "EndNode": 14, "Size": 10 },
            { "ID": 40, "Length": 64.62127695092323, "StartNode": 25, "EndNode": 26, "Size": 10 },
            { "ID": 41, "Length": 43.81576332816789, "StartNode": 26, "EndNode": 27, "Size": 10 },
            { "ID": 42, "Length": 52.28071466214949, "StartNode": 27, "EndNode": 28, "Size": 10 },
            { "ID": 43, "Length": 31.551126807446824, "StartNode": 28, "EndNode": 29, "Size": 10 },
            { "ID": 44, "Length": 53.12410279988288, "StartNode": 50, "EndNode": 44, "Size": 10 },
            { "ID": 45, "Length": 55.113663928325145, "StartNode": 44, "EndNode": 42, "Size": 10 },
            { "ID": 46, "Length": 73.0557822622117, "StartNode": 42, "EndNode": 41, "Size": 10 },
            { "ID": 47, "Length": 56.6360262215734, "StartNode": 41, "EndNode": 47, "Size": 10 },
            { "ID": 48, "Length": 53.49850481815351, "StartNode": 47, "EndNode": 46, "Size": 10 },
            { "ID": 49, "Length": 54.239288313092096, "StartNode": 46, "EndNode": 43, "Size": 10 },
            { "ID": 50, "Length": 58.03579214762693, "StartNode": 43, "EndNode": 45, "Size": 10 },
            { "ID": 51, "Length": 48.74194204108326, "StartNode": 45, "EndNode": 29, "Size": 10 },
            { "ID": 52, "Length": 71.21093740314902, "StartNode": 43, "EndNode": 36, "Size": 10 },
            { "ID": 53, "Length": 62.028722082038996, "StartNode": 36, "EndNode": 46, "Size": 10 },
            { "ID": 54, "Length": 45.925108057328394, "StartNode": 46, "EndNode": 37, "Size": 10 },
            { "ID": 55, "Length": 76.86806885833994, "StartNode": 45, "EndNode": 35, "Size": 10 },
            { "ID": 56, "Length": 98.71453064895931, "StartNode": 35, "EndNode": 26, "Size": 10 },
            { "ID": 57, "Length": 59.49070279945362, "StartNode": 49, "EndNode": 41, "Size": 10 },
            { "ID": 58, "Length": 110.69957384749839, "StartNode": 41, "EndNode": 10, "Size": 10 },
            { "ID": 59, "Length": 82.90710663746289, "StartNode": 10, "EndNode": 44, "Size": 10 },
            { "ID": 60, "Length": 115.59953977837132, "StartNode": 44, "EndNode": 48, "Size": 10 },
            { "ID": 61, "Length": 70.89583136961573, "StartNode": 42, "EndNode": 43, "Size": 10 },
            { "ID": 62, "Length": 81.78818078239053, "StartNode": 43, "EndNode": 50, "Size": 10 },
            { "ID": 63, "Length": 61.644055144311345, "StartNode": 50, "EndNode": 45, "Size": 10 },
            { "ID": 64, "Length": 56.974632996320814, "StartNode": 40, "EndNode": 47, "Size": 10 },
            { "ID": 65, "Length": 54.22220799819681, "StartNode": 47, "EndNode": 38, "Size": 10 }];
        callback(this.roads);
    };
    FakeApiClient.prototype.getPoi = function (callback) {
        var p = [{ "Name": "Piscina", "NodeID": 50, "Size": 60, "Description": "Splendida piscina immersa nel verde", "Icon": "piscina" },
            { "Name": "Palestra", "NodeID": 48, "Size": 60, "Description": "Palestra attrezzata, ottima per mantenerti in forma durante il soggiorno", "Icon": "palestra" },
            { "Name": "Spa", "NodeID": 23, "Size": 60, "Description": "La spa offre trattamenti rilassanti che renderanno il tuo soggiorno indimenticabile", "Icon": "spa" },
            { "Name": "Campo da golf", "NodeID": 10, "Size": 60, "Description": "Campo da golf adatto per giocatori di tutti i livelli", "Icon": "campo da golf" },
            { "Name": "Zona ristoro", "NodeID": 37, "Size": 60, "Description": "Consuma qui i tuoi pasti", "Icon": "zona ristoro" },
            { "Name": "Reception", "NodeID": 41, "Size": 60, "Description": "Aperta 24h", "Icon": "reception" },
            { "Name": "Campo da tennis", "NodeID": 15, "Size": 60, "Description": "Campo da tennis per professionisti", "Icon": "campo da tennis" }];
        this.poi = p;
        callback(this.poi);
    };
    FakeApiClient.prototype.parsingPoi = function (d) {
        var _apiClient = this;
        for (var i = 0; i < d.length; i++) {
            var url = d[i].Icon;
            /*  var p = $('<button>').attr({
                  type: "button",
                  class: "points",
                  name: d[i].Name,
                  style: "background:url(./images/" + url + ".svg) no-repeat scroll 0 0 transparent; width: 50; height: 50; border-color: transparent",
                  id: d[i].NodeID
              }).click(function() {
                  controller.onPoiPressed(parseInt(d[i].NodeID));
                  let data = //_apiClient.getPath(12, 34);
                      [{"ID":3,"Length":31.548427814119233,"StartNode":38,"EndNode":37,"Size":10},
                      {"ID":4,"Length":49.92009401091972,"StartNode":37,"EndNode":36,"Size":10},
                      {"ID":5,"Length":48.517700131736625,"StartNode":36,"EndNode":34,"Size":10,},
                      {"ID":6,"Length":32.67117857836385,"StartNode":34,"EndNode":35,"Size":10},
                      {"ID":7,"Length":34.40050281493861,"StartNode":35,"EndNode":33,"Size":10},
                      {"ID":8,"Length":82.8436743026555,"StartNode":33,"EndNode":32,"Size":10},
                      {"ID":9,"Length":28.52704821222271,"StartNode":32,"EndNode":31,"Size":10},
                      {"ID":10,"Length":28.027559559314543,"StartNode":31,"EndNode":30,"Size":10},
                      {"ID":11,"Length":56.39692384550201,"StartNode":30,"EndNode":25,"Size":10},
                      {"ID":12,"Length":34.466832341329166,"StartNode":25,"EndNode":24,"Size":10}];
                      console.log("Data: ", data);
                  controller.findPath(data);
                  $('.details').attr({
                      hidden: false,
                  }).text(d[i].Description);
              });
              $('#poiList').append(p); */
        }
    };
    /*
    public getPath(startNode: number, endNode: number, callback:(d: Array<RoadDto>) => void): void {
        console.log(startNode += 2);
        console.log(endNode += 5);
        this.path = //_apiClient.getPath(12, 34);
            [{"ID":3,"Length":31.548427814119233,"StartNode":38,"EndNode":37,"Size":10},
            {"ID":4,"Length":49.92009401091972,"StartNode":37,"EndNode":36,"Size":10},
            {"ID":5,"Length":48.517700131736625,"StartNode":36,"EndNode":34,"Size":10,},
            {"ID":6,"Length":32.67117857836385,"StartNode":34,"EndNode":35,"Size":10},
            {"ID":7,"Length":34.40050281493861,"StartNode":35,"EndNode":33,"Size":10},
            {"ID":8,"Length":82.8436743026555,"StartNode":33,"EndNode":32,"Size":10},
            {"ID":9,"Length":28.52704821222271,"StartNode":32,"EndNode":31,"Size":10},
            {"ID":10,"Length":28.027559559314543,"StartNode":31,"EndNode":30,"Size":10},
            {"ID":11,"Length":56.39692384550201,"StartNode":30,"EndNode":25,"Size":10},
            {"ID":12,"Length":34.466832341329166,"StartNode":25,"EndNode":24,"Size":10}];
        callback(this.path);
    }
*/
    FakeApiClient.prototype.getNodeFromID = function (id) {
        var node;
        this.nodes.forEach(function (n) {
            if (n.Id == id) {
                node = n;
            }
        });
        return node;
    };
    FakeApiClient.prototype.getPath = function (startNode, endNode, callback) {
        var _this = this;
        var s = new list_1.List();
        var t = new list_1.List();
        var f = new array_map_1.ArrayMap();
        var j = new array_map_1.ArrayMap();
        var neighbors = new list_1.List();
        var distance;
        var nearest;
        this.nodes.forEach(function (n) {
            f.push(n, Number.POSITIVE_INFINITY);
            j.push(n, undefined);
        });
        var tempStartNode = this.getNodeFromID(startNode);
        var tempEndNode = this.getNodeFromID(endNode);
        f.replace(tempStartNode, tempStartNode, 0);
        this.getNeighbor(tempStartNode).list().forEach(function (n) {
            f.replace(n, n, _this.getRoadBetweenNodes(tempStartNode, n).Length);
        });
        t.addAll(this.nodes);
        t.remove(tempStartNode);
        while (!t.isEmpty()) {
            nearest = this.getNearest(f, t);
            if (this.getNeighbor(tempStartNode).contains(nearest)) {
                j.replace(nearest, nearest, tempStartNode);
            }
            t.remove(nearest);
            s.add(nearest);
            neighbors = this.getNeighbor(nearest);
            neighbors.removeAll(s.list());
            neighbors.list().forEach(function (n) {
                distance = f.getSecond(nearest) + _this.getRoadBetweenNodes(nearest, n).Length;
                if (distance < f.getSecond(n)) {
                    f.replace(n, n, distance);
                    j.replace(n, n, nearest);
                }
            });
        }
        var pathFound = new list_1.List();
        var next = tempEndNode;
        var prev;
        do {
            prev = j.getSecond(next);
            pathFound.add(this.getRoadBetweenNodes(next, prev));
            next = prev;
        } while (next != tempStartNode);
        this.path = pathFound.list();
        callback(this.path);
    };
    FakeApiClient.prototype.getRoadBetweenNodes = function (n1, n2) {
        var road;
        this.getRoadsFromNode(n1).list().forEach(function (r) {
            if (r.StartNode === n2.Id || r.EndNode === n2.Id) {
                road = r;
            }
        });
        return road;
    };
    FakeApiClient.prototype.getRoadsFromNode = function (node) {
        var roadVet = new list_1.List();
        this.roads.forEach(function (r) {
            if (r.StartNode === node.Id || r.EndNode === node.Id) {
                roadVet.add(r);
            }
        });
        return roadVet;
    };
    FakeApiClient.prototype.getNeighbor = function (node) {
        var _this = this;
        var neighbors = new list_1.List();
        this.getRoadsFromNode(node).list().forEach(function (r) {
            neighbors.add(r.StartNode === node.Id ? _this.getNodeFromID(r.EndNode) : _this.getNodeFromID(r.StartNode));
        });
        return neighbors;
    };
    FakeApiClient.prototype.getNearest = function (map, t) {
        var n;
        var distance = Number.POSITIVE_INFINITY;
        t.list().forEach(function (p) {
            if (map.getSecond(p) < distance) {
                n = p;
                distance = map.getSecond(p);
            }
        });
        return n;
    };
    return FakeApiClient;
}());
exports.FakeApiClient = FakeApiClient;
