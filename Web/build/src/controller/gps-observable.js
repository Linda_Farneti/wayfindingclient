"use strict";
exports.__esModule = true;
var pair_impl_1 = require("../utility/pair-impl");
var GpsObservable = /** @class */ (function () {
    function GpsObservable() {
        gpsObservable = this;
    }
    GpsObservable.prototype.setup = function (d) {
        this.observer = d;
    };
    GpsObservable.prototype.getObserver = function () {
        return this.observer;
    };
    GpsObservable.prototype.onStart = function () {
        this.observer.onPositionUpdate(this.latitude, this.longitude);
    };
    GpsObservable.prototype.onCoordinatesChanged = function (lat, lon) {
        this.latitude = lat;
        this.longitude = lon;
        this.observer.onPositionUpdate(this.latitude, this.longitude);
    };
    GpsObservable.prototype.getActualPosition = function () {
        return new pair_impl_1.PairImpl(this.latitude, this.longitude);
    };
    GpsObservable.getInstance = function () {
        if (this.instance == null || this.instance == undefined) {
            this.instance = new GpsObservable();
        }
        return this.instance;
    };
    return GpsObservable;
}());
exports.GpsObservable = GpsObservable;
