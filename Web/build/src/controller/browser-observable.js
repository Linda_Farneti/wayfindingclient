"use strict";
exports.__esModule = true;
var list_1 = require("../utility/list");
var BrowserObservable = /** @class */ (function () {
    function BrowserObservable() {
        this.observers = new list_1.List();
    }
    BrowserObservable.prototype.setup = function () {
        var _this = this;
        $("#b_search").click(function () {
            _this.getObservers().list().forEach(function (obs) {
                obs.onSearchPressed($("#search").val());
            });
        });
        $("#start").click(function () {
            _this.getObservers().list().forEach(function (obs) {
                obs.onStartPressed();
            });
        });
        $("#search").change(function () {
            if ($("#search").val() != null && $("#search").val() != undefined) {
                var result_1;
                $("#resultList").empty();
                $("#resultList").show();
                _this.getObservers().list().forEach(function (obs) {
                    result_1 = obs.onSearchPressed($("#search").val());
                    result_1.forEach(function (r) {
                        $('#resultList').append($('<option>', {
                            "class": "result",
                            id: "result",
                            text: r.getX() + r.getY(),
                            val: r.getX()
                        }));
                    });
                });
            }
            else {
                $("#resultList").hide();
            }
        });
        $(".resultList").change(function () {
            _this.getObservers().list().forEach(function (obs) {
                obs.onOptionClicked($("#resultList :selected").val());
            });
        });
    };
    BrowserObservable.prototype.addObserver = function (obs) {
        this.observers.add(obs);
    };
    BrowserObservable.prototype.getObservers = function () {
        return this.observers;
    };
    BrowserObservable.prototype.setMap = function (map) {
        this.map = map;
        //per vedere le coordinate cliccate (fake gps)
        /*(this.map).on("click", (e:any) => {
            console.log("Click on: ", e.latlng);
        });*/
    };
    BrowserObservable.getInstance = function () {
        if (this.instance === null || this.instance === undefined) {
            this.instance = new BrowserObservable();
        }
        return this.instance;
    };
    BrowserObservable.prototype.loadingScreenOn = function () {
        document.getElementById("loading_screen").style.display = 'block';
    };
    BrowserObservable.prototype.loadingScreenOff = function () {
        document.getElementById("loading_screen").style.display = 'none';
    };
    return BrowserObservable;
}());
exports.BrowserObservable = BrowserObservable;
