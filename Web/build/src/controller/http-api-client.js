"use strict";
exports.__esModule = true;
var consts_1 = require("../utility/consts");
var HttpApiClient = /** @class */ (function () {
    function HttpApiClient(c, s) {
        this.controller = c;
        this.path = s;
        this.id = consts_1.Consts.MAP_ID;
    }
    HttpApiClient.prototype.getMap = function (callback) {
        $.ajax({
            url: this.path + "Api/Map/GetMapFromID?ID=" + this.id,
            data: null,
            success: function (d) {
                callback(d);
            },
            dataType: "json"
        });
    };
    HttpApiClient.prototype.getNodes = function (callback) {
        $.ajax({
            url: this.path + "Api/Node/GetNodes?MapID=" + this.id,
            data: null,
            success: function (d) {
                callback(d);
            },
            dataType: "json"
        });
    };
    HttpApiClient.prototype.getRoads = function (callback) {
        var data;
        $.ajax({
            url: this.path + "Api/Road/GetRoads?MapID=" + this.id,
            data: null,
            success: function (d) {
                callback(d);
            },
            dataType: "json"
        });
    };
    HttpApiClient.prototype.getPoi = function (callback) {
        var _this = this;
        var dataPoi;
        $.ajax({
            url: this.path + "Api/Point/GetPoints?MapID=" + this.id,
            data: null,
            success: function (d) {
                dataPoi = _this.parsingPoi(d);
                callback(d);
            },
            dataType: "json"
        });
    };
    HttpApiClient.prototype.getPath = function (startNode, endNode, callback) {
        var myUrl = this.path + "Api/Road/GetPath?FirstNode=" + startNode + "&SecondNode=" + endNode + "&MapID=" + this.id;
        $.ajax({
            type: "get",
            url: myUrl,
            data: null,
            success: function (d) {
                callback(d);
                //this.controller.findPath(data);
            },
            dataType: "json"
        });
    };
    HttpApiClient.prototype.parsingPoi = function (d) {
        for (var i = 0; i < d.length; i++) {
            var poi = d[i];
            /* var p = $('</br><button>').attr({
                 type: "button",
                 class: "points",
                 name: poi.Name,
                 id: poi.NodeID
             }).text(poi.Name).click(() => {
                 var pos: Array<number> = controller.onPoiPressed(parseInt(poi.NodeID));
                 $.ajax ({
                     type: "get",
                     url: this.path + "/Road/GetPath", //?FirstNode=12&SecondNode=34",
                     contentType: "application/json",
                     data: JSON.stringify({FirstNode:pos[0],SecondNode:pos[1]}),
                     success: (i)=> {
                         d = i;
                         controller.findPath(d);
                         $('.details').attr({
                             hidden: false,
                         }).text(poi.Description);
                     },
                     dataType: "json"
                 })
             });
             $('#poiList').append(p);
             */
        }
        return d;
    };
    return HttpApiClient;
}());
exports.HttpApiClient = HttpApiClient;
