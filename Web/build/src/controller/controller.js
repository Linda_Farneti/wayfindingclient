"use strict";
exports.__esModule = true;
var Controller = /** @class */ (function () {
    function Controller(gpsObs) {
        this.gps = gpsObs;
    }
    Controller.prototype.setDrawer = function (d) {
        this.drawer = d;
    };
    Controller.prototype.setApiClient = function (a) {
        this.apiClient = a;
    };
    Controller.prototype.onSearchPressed = function (id) {
        var arr = this.drawer.deletePoi(id);
        return arr;
    };
    Controller.prototype.onStartPressed = function () {
        this.gps.onStart();
    };
    Controller.prototype.onPoiClicked = function (p, callback) {
        var nodes = this.drawer.poiPressed(p);
        var roads;
        console.log("Richiesta percorso da: ", nodes.getX().Id, " a: ", nodes.getY().NodeID);
        this.findPath(nodes.getX().Id, nodes.getY().NodeID);
    };
    Controller.prototype.findPath = function (id1, id2) {
        var _this = this;
        this.apiClient.getPath(id1, id2, function (r) {
            _this.drawer.findPath(r);
        });
    };
    Controller.prototype.viewAll = function () {
        this.drawer.viewAll();
    };
    Controller.prototype.onOptionClicked = function (s) {
        //this.drawer.optionClicked(s);
        console.log("Metodo onOptionClicked del controller chiamato. BIZZARRO");
    };
    return Controller;
}());
exports.Controller = Controller;
