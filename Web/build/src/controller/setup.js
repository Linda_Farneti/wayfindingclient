"use strict";
exports.__esModule = true;
var L = require("leaflet");
var consts_1 = require("../utility/consts");
var browser_observable_1 = require("./browser-observable");
var map_drawer_1 = require("../view/map-drawer");
var controller_1 = require("./controller");
var http_api_client_1 = require("./http-api-client");
var gps_observable_1 = require("./gps-observable");
var Setup = /** @class */ (function () {
    function Setup() {
    }
    Setup.prototype.setup = function () {
        var _this = this;
        var b = browser_observable_1.BrowserObservable.getInstance();
        //b.loadingScreenOn();
        this.controller = new controller_1.Controller(this.gps);
        this.apiClient = new http_api_client_1.HttpApiClient(this.controller, "http://" + consts_1.Consts.IP_SERVER + "/Wayfinding/");
        //this.apiClient = new FakeApiClient(this.controller);
        this.apiClient.getMap(function (m) {
            _this.map = L.map('map').setView([m.Latitude, m.Longitude], m.Zoom);
            _this.apiClient.getNodes(function (n) {
                _this.apiClient.getRoads(function (r) {
                    _this.apiClient.getPoi(function (p) {
                        _this.gps = gps_observable_1.GpsObservable.getInstance();
                        _this.drawer = new map_drawer_1.MapDrawer(_this.map, new Array(n, r, p));
                        _this.controller.setDrawer(_this.drawer);
                        _this.controller.setApiClient(_this.apiClient);
                        _this.drawer.addObserver(_this.controller);
                        b.addObserver(_this.controller);
                        b.setMap(_this.map);
                        b.setup();
                        _this.gps.setup(_this.drawer);
                        //b.loadingScreenOff();
                    });
                });
            });
        });
    };
    return Setup;
}());
$(document).ready(function () {
    new Setup().setup();
});
