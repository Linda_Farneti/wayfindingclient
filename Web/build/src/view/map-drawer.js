"use strict";
exports.__esModule = true;
var L = require("leaflet");
var array_map_1 = require("../utility/array-map");
var list_1 = require("../utility/list");
var pair_impl_1 = require("../utility/pair-impl");
var distance_calculator_1 = require("../utility/distance-calculator");
var gps_observable_1 = require("../controller/gps-observable");
var consts_1 = require("../utility/consts");
var MapDrawer = /** @class */ (function () {
    //private gps: GpsObserver;
    function MapDrawer(map, data) {
        this.draggableMap = true;
        this.nodes = new Array();
        this.roads = new array_map_1.ArrayMap();
        //this.poi = new ArrayMap<PoiDto, L.Circle>();
        this.poi = new array_map_1.ArrayMap();
        this.path = new array_map_1.ArrayMap();
        this.map = map;
        this.drawMap(data);
        this.mapObserver = new list_1.List();
        this.gps = gps_observable_1.GpsObservable.getInstance();
        //let actualPos = this.gps.getActualPosition().getX();
        /*
        let actualLat = this.gps.getActualLatitude();
        $("#utile").text("Lat: " + actualLat);
        let actualLong = this.gps.getActualLongitude();
        this.setNewPosition(actualLat, actualLong);
        */
    }
    MapDrawer.prototype.getActualPosition = function () {
        var node;
        node = this.nearestNode(this.gps.getActualPosition().getX(), this.gps.getActualPosition().getY());
        //node = this.nearestNode(this.nodes[0].Latitude, this.nodes[0].Longitude);
        return node;
    };
    MapDrawer.prototype.drawPoi = function (c) {
        var _this = this;
        var coord = Array();
        coord.push(this.getCoordinates(c.NodeID));
        /*  let newCircle = L.circle(coord[0], {
            color: "grey",
            fillColor: "white",
            radius: 8,
            fillOpacity: 0.8,
        }).addTo(this.map);

        newCircle.on("click", () => {
            this.resetPath();
            this.mapObserver.list().forEach(obs => {
                obs.onPoiClicked(c, (r) => {
                    r.forEach(p => {
                        this.underlineRoad(p);
                    });
                });
                
            })
        });  */
        var icon = L.icon({
            iconUrl: "http://" + consts_1.Consts.IP_SERVER + "/Wayfinding/Images/" + c.Icon + ".svg",
            //iconUrl: "http://" + Consts.IP_CLIENT "/images/" + c.Icon + ".svg",
            iconSize: [c.Size, c.Size]
        });
        var marker = L.marker(coord[0]);
        marker.setIcon(icon);
        marker.addTo(this.map);
        marker.on("click", function () {
            _this.optionClicked(c);
        });
        //this.poi.push(c, newCircle);
        this.poi.push(c, marker);
    };
    MapDrawer.prototype.optionClicked = function (poi) {
        var _this = this;
        //let poi = this.searchPoiFromName(s);
        this.mapObserver.list().forEach(function (obs) {
            var r = obs.onPoiClicked(poi, function (r) {
                /*r.forEach(p => {
                    this.underlineRoad(p);
                });*/
                _this.findPath(r);
            });
        });
    };
    MapDrawer.prototype.searchPoiFromName = function (s) {
        var res;
        this.poi.entries().list().forEach(function (p) {
            if (p.getX().Name == s) {
                res = p.getX();
            }
        });
        return res;
    };
    MapDrawer.prototype.nearestNode = function (lat, long) {
        var node;
        var minDist = Number.POSITIVE_INFINITY;
        this.nodes.forEach(function (n) {
            var dist = distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(lat, long, n);
            if (dist < minDist) {
                minDist = dist;
                node = n;
            }
        });
        return node;
    };
    MapDrawer.prototype.drawRoad = function (r) {
        var coord = Array();
        coord.push(this.getCoordinates(r.StartNode));
        coord.push(this.getCoordinates(r.EndNode));
        var newLine = L.polyline(coord, {
            color: "white",
            weight: r.Size
        }).addTo(this.map);
        this.roads.push(r, newLine);
        //per vedere i nodi corrispondenti (fake gps)
        /*newLine.on("click", () => {
            console.log("Line clicked! Start:", r.StartNode, " End:", r.EndNode);
        })*/
    };
    MapDrawer.prototype.resetPath = function () {
        var _this = this;
        this.poi.entries().list().forEach(function (p) {
            if (p.getX().NodeID != _this.poiSelected) {
                //cambiare lo stile dell'icona selezionata
                /*  p.getY().setStyle({
                      color: "grey"
                  }) */
            }
        });
        this.path.entries().list().forEach(function (r) {
            _this.map.removeLayer(r.getY());
        });
        this.path = new array_map_1.ArrayMap();
    };
    MapDrawer.prototype.getCoordinates = function (id) {
        var coord = new Array();
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].Id === id) {
                coord = ([this.nodes[i].Latitude, this.nodes[i].Longitude]);
            }
        }
        return coord;
    };
    MapDrawer.prototype.changeStylePoi = function (id) {
        this.poi.entries().list().forEach(function (p) {
            if (p.getX().NodeID != id.NodeID) {
                /*  p.getY().setStyle({
                      color: "grey",
                  })
              } else {
                  p.getY().setStyle({
                      color: "blue",
                  }) */
            }
        });
    };
    MapDrawer.prototype.underlineRoad = function (r) {
        var coord = Array();
        coord.push(this.getCoordinates(r.StartNode));
        coord.push(this.getCoordinates(r.EndNode));
        var newLine = L.polyline(coord, {
            color: "green",
            //ashArray: '20.15',
            lineJoin: 'round',
            weight: 5,
            opacity: 1
        }).addTo(this.map);
        this.path.push(r, newLine);
    };
    MapDrawer.prototype.searchPolyFromStartNode = function (id) {
        var pol;
        this.path.entries().list().forEach(function (p) {
            if (p.getX().StartNode == id || p.getX().EndNode == id) {
                pol = p.getY();
            }
        });
        return pol;
    };
    MapDrawer.prototype.updatePath = function (lat, long) {
        /* let t: boolean;
         let coord = Array();
         coord.push([lat, long]);
         this.path.entries().list().forEach(p => {
             if (p.getX().StartNode == this.lastCrossed.getX().Id) {
                 t = true;
             }
             if (t == true) {
                 coord.push(p.getY().getLatLngs()[1]);
             }
         });
         this.hidePath();
         this.lastCrossed.getY().setLatLngs(coord).setStyle({
             color: "green"
         });
         this.lastCrossed.getY().bringToFront(); */
    };
    MapDrawer.prototype.hidePath = function () {
        this.path.entries().list().forEach(function (p) {
            p.getY().setStyle({
                color: "white"
            });
        });
    };
    MapDrawer.prototype.drawMap = function (data) {
        var _this = this;
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);
        data[0].forEach(function (n) {
            var newPoint = { Id: n.Id, Latitude: n.Latitude, Longitude: n.Longitude };
            _this.nodes.push(newPoint);
        });
        data[1].forEach(function (r) {
            _this.drawRoad(r);
        });
        data[2].forEach(function (p) {
            _this.drawPoi(p);
        });
    };
    MapDrawer.prototype.poiPressed = function (p) {
        this.changeStylePoi(p);
        this.poiSelected = p.NodeID;
        return new pair_impl_1.PairImpl(this.getActualPosition(), p);
    };
    MapDrawer.prototype.findPath = function (data) {
        var _this = this;
        this.resetPath();
        data.forEach(function (n) {
            _this.underlineRoad(n);
        });
        for (var i = 0; data[i] != null; i++) {
            this.underlineRoad(data[i]);
        }
        this.setNewPosition(this.getActualPosition().Latitude, this.getActualPosition().Longitude);
        //this.lastCrossed = new PairImpl(this.nodes[0], this.path.entries().list()[0].getY());
    };
    MapDrawer.prototype.searchNodeFromId = function (id) {
        var n;
        for (var i = 0; i < this.nodes.length; i++) {
            if (this.nodes[i].Id == id) {
                n = this.nodes[i];
            }
        }
        return n;
    };
    MapDrawer.prototype.setNewPosition = function (x, y) {
        if (this.pointSelector != undefined && this.pointSelector != null) {
            this.map.removeLayer(this.pointSelector);
        }
        var pointIcon = L.icon({
            iconUrl: "./images/positionMarker2.png",
            iconSize: [60, 60]
        });
        this.pointSelector = L.marker([x, y]);
        this.pointSelector.setIcon(pointIcon);
        this.pointSelector.addTo(this.map);
    };
    MapDrawer.prototype.onPositionUpdate = function (x, y) {
        this.setNewPosition(x, y);
        for (var i = 0; i < this.nodes.length; i++) {
            //da sistemare in base all'algoritmo di correzione
            //if (this.nearestNode(x, y) == this.nodes[i] && this.nodes[i] != this.lastCrossed.getX()) {
            if (this.nodes[i].Latitude == x && this.nodes[i].Longitude == y) {
                //this.lastCrossed = new PairImpl(this.nodes[i],this.searchPolyFromStartNode(this.nodes[i].Id));
            }
        }
        this.updatePath(x, y);
    };
    MapDrawer.prototype.deletePoi = function (id) {
        var _this = this;
        var arr = new Array();
        this.resetPath();
        this.poi.entries().list().forEach(function (p) {
            p.getY().addTo(_this.map);
            //bisogna scegliere come fare le ricerche, per ora risultano anche con singole lettere
            if (p.getX().Name.toLowerCase().indexOf(id) < 0) {
                //if (p.getX().Name.substr(0, id.length).toLowerCase() != id) {
                _this.map.removeLayer(p.getY());
            }
            else {
                //metterci la distanza vera che ci separa
                arr.push(new pair_impl_1.PairImpl(p.getX().Name, "(" + distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(_this.getCoordinates(p.getX().NodeID)[0], _this.getCoordinates(p.getX().NodeID)[1], _this.getActualPosition()) + "km)"));
            }
        });
        return arr;
    };
    MapDrawer.prototype.viewAll = function () {
        var _this = this;
        this.poi.entries().list().forEach(function (p) {
            p.getY().addTo(_this.map);
        });
    };
    MapDrawer.prototype.addObserver = function (obs) {
        this.mapObserver.add(obs);
    };
    return MapDrawer;
}());
exports.MapDrawer = MapDrawer;
