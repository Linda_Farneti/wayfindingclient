"use strict";
exports.__esModule = true;
var State;
(function (State) {
    State[State["DRAW_NODE"] = 0] = "DRAW_NODE";
    State[State["SELECT_PATH"] = 1] = "SELECT_PATH";
    State[State["SELECTION"] = 2] = "SELECTION";
    State[State["MOVE"] = 3] = "MOVE";
})(State = exports.State || (exports.State = {}));
;
var Item;
(function (Item) {
    Item[Item["ROAD"] = 0] = "ROAD";
    Item[Item["ICON"] = 1] = "ICON";
    Item[Item["NODE"] = 2] = "NODE";
})(Item = exports.Item || (exports.Item = {}));
;
var Consts = /** @class */ (function () {
    function Consts() {
    }
    Consts.LATITUDE = 44.40262;
    Consts.LONGITUDE = 11.50949;
    Consts.CIRCLE_RAY = 10;
    Consts.MAP_FOCUS = 17;
    Consts.ROAD_WIDTH = 10;
    Consts.OPACITY = 1;
    Consts.DEFAULT_ICON_SIZE = 20;
    Consts.ICON_PATH = "./src/view/images/";
    Consts.ICON_EXT = ".svg";
    Consts.IP_SERVER = "192.168.1.5";
    Consts.IP_CLIENT = "127.0.0.1:5500";
    Consts.MAP_ID = 87;
    Consts.DEFAULT_NODE_STYLE = {
        color: "grey"
    };
    Consts.DEFAULT_ROAD_STYLE = {
        color: "white"
    };
    Consts.SELECTION_STYLE = {
        color: "blue"
    };
    return Consts;
}());
exports.Consts = Consts;
