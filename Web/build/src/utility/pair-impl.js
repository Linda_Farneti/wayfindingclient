"use strict";
exports.__esModule = true;
var PairImpl = /** @class */ (function () {
    function PairImpl(f, s) {
        this.first = f;
        this.second = s;
    }
    PairImpl.prototype.getX = function () {
        return this.first;
    };
    PairImpl.prototype.getY = function () {
        return this.second;
    };
    PairImpl.prototype.equals = function (x1, y1) {
        return this.first === x1 && this.second === y1;
    };
    return PairImpl;
}());
exports.PairImpl = PairImpl;
