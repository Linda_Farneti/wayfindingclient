"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var list_1 = require("./list");
var array_map_1 = require("./array-map");
var nodes_1 = require("./nodes");
var roads_1 = require("./roads");
var PathCalculator = /** @class */ (function () {
    function PathCalculator() {
    }
    PathCalculator.getPath = function (startNode, endNode, nodes, roads) {
        var s = new list_1.List();
        var t = new list_1.List();
        var f = new array_map_1.ArrayMap();
        var j = new array_map_1.ArrayMap();
        var neighbors = new list_1.List();
        var distance;
        var nearest;
        nodes.forEach(function (n) {
            f.push(n, Number.POSITIVE_INFINITY);
            j.push(n, undefined);
        });
        var tempStartNode = nodes_1.Nodes.getNodeFromID(startNode, nodes);
        var tempEndNode = nodes_1.Nodes.getNodeFromID(endNode, nodes);
        f.replace(tempStartNode, tempStartNode, 0);
        this.getNeighbour(tempStartNode, nodes, roads).list().forEach(function (n) {
            f.replace(n, n, roads_1.Roads.getRoadBetweenNodes(tempStartNode, n, roads).Length);
        });
        t.addAll(nodes);
        t.remove(tempStartNode);
        while (!t.isEmpty()) {
            nearest = this.getNearest(f, t);
            if (this.getNeighbour(tempStartNode, nodes, roads).contains(nearest)) {
                j.replace(nearest, nearest, tempStartNode);
            }
            t.remove(nearest);
            s.add(nearest);
            neighbors = this.getNeighbour(nearest, nodes, roads);
            neighbors.removeAll(s.list());
            neighbors.list().forEach(function (n) {
                distance = f.getSecond(nearest) + roads_1.Roads.getRoadBetweenNodes(nearest, n, roads).Length;
                if (distance < f.getSecond(n)) {
                    f.replace(n, n, distance);
                    j.replace(n, n, nearest);
                }
            });
        }
        var pathFound = new list_1.List();
        var next = tempEndNode;
        var prev;
        do {
            prev = j.getSecond(next);
            pathFound.add(roads_1.Roads.getRoadBetweenNodes(next, prev, roads));
            next = prev;
        } while (next != tempStartNode);
        var path;
        path = pathFound.list();
        return path;
    };
    PathCalculator.getNeighbour = function (node, nodes, roads) {
        var neighbors = new list_1.List();
        roads_1.Roads.getRoadsFromNode(node, roads).list().forEach(function (r) {
            neighbors.add(r.StartNode === node.Id ? nodes_1.Nodes.getNodeFromID(r.EndNode, nodes) : nodes_1.Nodes.getNodeFromID(r.StartNode, nodes));
        });
        return neighbors;
    };
    PathCalculator.getNearest = function (map, t) {
        var n;
        var distance = Number.POSITIVE_INFINITY;
        t.list().forEach(function (p) {
            if (map.getSecond(p) < distance) {
                n = p;
                distance = map.getSecond(p);
            }
        });
        return n;
    };
    return PathCalculator;
}());
exports.PathCalculator = PathCalculator;
