"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var TYPES = {
    IApiClient: Symbol("IApiClient"),
    CommandObserver: Symbol("CommandObserver"),
    MapObserver: Symbol("MapObserver"),
    GpsObserver: Symbol("GpsObserver"),
    Drawer: Symbol("Drawer"),
    BrowserObservable: Symbol("BrowserObservable")
};
exports.default = TYPES;
