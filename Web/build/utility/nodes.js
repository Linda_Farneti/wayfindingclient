"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var distance_calculator_1 = require("./distance-calculator");
var Nodes = /** @class */ (function () {
    function Nodes() {
    }
    Nodes.getNodeFromID = function (id, nodes) {
        var node;
        nodes.forEach(function (n) {
            if (n.Id == id) {
                node = n;
            }
        });
        return node;
    };
    Nodes.getCoordinates = function (id, nodes) {
        var coord = new Array();
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].Id === id) {
                coord = ([nodes[i].Latitude, nodes[i].Longitude]);
            }
        }
        return coord;
    };
    Nodes.nearestNode = function (lat, long, nodes) {
        var node;
        var minDist = Number.POSITIVE_INFINITY;
        nodes.forEach(function (n) {
            var dist = distance_calculator_1.DistanceCalculator.getDistanceNodeToNode(lat, long, n.Latitude, n.Longitude);
            if (dist < minDist) {
                minDist = dist;
                node = n;
            }
        });
        return node;
    };
    return Nodes;
}());
exports.Nodes = Nodes;
