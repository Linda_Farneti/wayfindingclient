"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var list_1 = require("./list");
var Roads = /** @class */ (function () {
    function Roads() {
    }
    //Per ora nel programma non viene utilizzata. Controllare
    /*static searchPolyFromStartNode(id: number, map: Map<RoadDto, L.Polyline>): L.Polyline {
        let pol: L.Polyline;
        map.entries().list().forEach(p => {
            if (p.getX().StartNode == id || p.getX().EndNode == id) {
                pol = p.getY();
            }
        });
        return pol;
    }*/
    Roads.getRoadsFromNode = function (node, roads) {
        var roadVet = new list_1.List();
        roads.forEach(function (r) {
            if (r.StartNode === node.Id || r.EndNode === node.Id) {
                roadVet.add(r);
            }
        });
        return roadVet;
    };
    Roads.getRoadBetweenNodes = function (n1, n2, roads) {
        var road;
        this.getRoadsFromNode(n1, roads).list().forEach(function (r) {
            if (r.StartNode === n2.Id || r.EndNode === n2.Id) {
                road = r;
            }
        });
        return road;
    };
    return Roads;
}());
exports.Roads = Roads;
