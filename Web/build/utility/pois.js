"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//test non implementato perchè per ora non viene utilizzata
var Pois = /** @class */ (function () {
    function Pois() {
    }
    Pois.searchPoiFromName = function (s, poi) {
        var res;
        poi.entries().list().forEach(function (p) {
            if (p.getX().Name == s) {
                res = p.getX();
            }
        });
        return res;
    };
    return Pois;
}());
exports.Pois = Pois;
