"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var nodes_1 = require("./nodes");
var inversify_config_1 = require("./inversify.config");
var types_1 = require("./types");
var Position = /** @class */ (function () {
    function Position() {
    }
    Position.getPosition = function (nodes) {
        var _gps = inversify_config_1.default.get(types_1.default.GpsObserver);
        var node;
        node = nodes_1.Nodes.nearestNode(_gps.getActualPosition().getX(), _gps.getActualPosition().getY(), nodes);
        return node;
    };
    return Position;
}());
exports.Position = Position;
