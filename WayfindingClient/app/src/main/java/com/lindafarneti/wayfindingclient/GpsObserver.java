package com.lindafarneti.wayfindingclient;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import static android.content.ContentValues.TAG;

public class GpsObserver implements  IGpsObserver {

    Context mContext;
    WebView wb;

    /** Instantiate the interface and set the context */
    GpsObserver(Context c, WebView w) {
        mContext = c;
        wb = w;
    }

    @Override
    public void onCoordinatesChanged(double lat, double lon) {
        Log.e("TEST", "GPS Observer: "+lat+", "+lon);
        wb.loadUrl("javascript:onCoordinatesChanged("+lat+", "+lon+")");
    }


    //Probabilmente da cancellare
    @JavascriptInterface
    public void showToast(String toast) {
       // Log.i("Check", toast);
        Toast.makeText(mContext, toast, Toast.LENGTH_LONG).show();
    }

}
