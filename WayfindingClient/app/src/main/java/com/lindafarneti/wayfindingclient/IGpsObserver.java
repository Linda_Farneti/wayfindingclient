package com.lindafarneti.wayfindingclient;

public interface IGpsObserver {
    void onCoordinatesChanged(double lat, double lon);
}