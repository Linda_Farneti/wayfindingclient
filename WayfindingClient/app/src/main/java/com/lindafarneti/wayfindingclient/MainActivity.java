package com.lindafarneti.wayfindingclient;
import android.Manifest;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

import static com.lindafarneti.wayfindingclient.Consts.MY_PERMISSIONS_REQUEST_COARSE_LOCATION;
import static com.lindafarneti.wayfindingclient.Consts.MY_PERMISSIONS_REQUEST_FINE_LOCATION;

public class MainActivity extends AppCompatActivity {

    private Gps gps;
    private GpsObserver gObs;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView myWebView = (WebView) findViewById(R.id.webview);
        if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        myWebView.loadUrl(Consts.APP_URL);

        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webView = (WebView) findViewById(R.id.webview);


        // valutare se bisogna inserire il controllo sull'attuale versione di android
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            }
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST_COARSE_LOCATION);
            }
        } else {
            startGps();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Consts.MY_PERMISSIONS_REQUEST_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e("TEST", "COARSE granted");
                    startGps();
                } else {
                    Log.e("TEST", "COARSE REFUSED");
                }
                return;
            }

            case Consts.MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e("TEST", "FINE granted");
                    startGps();
                } else {
                    Log.e("TEST", "FINE REFUSED");
                }
                return;
            }
        }
    }


    private void startGps() {
        gObs = new GpsObserver(this, webView);
        gps = new Gps(this);
        gps.addObserver(gObs);
        webView.addJavascriptInterface(gObs, "AndroidBridge");
        gps.start();
    }

}

